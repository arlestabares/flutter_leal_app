abstract class GlobalRemoteDataSource<Type> {
  Future<Type> getMovie();
  Future<Type> getDetailsById(int id);
  Future<Type> getSeasons(int id, int seasonNumber);
  Future<Type> getEpisode(int id, int seasonNumber, int episodeNumber);
}
