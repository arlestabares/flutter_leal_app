import 'dart:convert';

MovieDetailsModel movieDetailsFromJson(String str) =>
    MovieDetailsModel.fromJson(json.decode(str));

class MovieDetailsModel {
  MovieDetailsModel({
    this.id,
    this.name,
    this.type,
    this.adult,
    this.status,
    this.seasons,
    this.tagline,
    this.overview,
    this.homepage,
    this.voteCount,
    this.languages,
    // this.createdBy,
    this.popularity,
    this.posterPath,
    this.voteAverage,
    this.lastAirDate,
    this.inProduction,
    this.backdropPath,
    this.originalName,
    this.firstAirDate,
    this.originCountry,
    this.episodeRunTime,
    this.numberOfSeasons,
    this.lastEpisodeToAir,
    this.nextEpisodeToAir,
    this.numberOfEpisodes,
    this.originalLanguage,
  });
  String? uniqueId = '';
  //
  int? id;
  bool? adult;
  String? name;
  String? type;
  String? status;
  int? voteCount;
  String? tagline;
  String? homepage;
  String? overview;
  String? posterPath;
  double? popularity;
  bool? inProduction;
  double? voteAverage;
  String? originalName;
  String? backdropPath;
  int? numberOfSeasons;
  List<Season>? seasons;
  int? numberOfEpisodes;
  DateTime? lastAirDate;
  DateTime? firstAirDate;
  List<String>? languages;
  // List<dynamic>? createdBy;
  String? originalLanguage;
  dynamic nextEpisodeToAir;
  List<int>? episodeRunTime;
  List<String>? originCountry;
  Episode? lastEpisodeToAir;

  factory MovieDetailsModel.fromJson(Map<String, dynamic> json) =>
      MovieDetailsModel(
        id: json["id"],
        type: json["type"],
        name: json["name"],
        adult: json["adult"],
        status: json["status"],
        tagline: json["tagline"],
        homepage: json["homepage"],
        overview: json["overview"],
        voteCount: json["vote_count"],
        posterPath: json["poster_path"],
        popularity: json["popularity"] / 1,
        originalName: json["original_name"],
        inProduction: json["in_production"],
        backdropPath: json["backdrop_path"],
        voteAverage: json["vote_average"] / 1,
        originalLanguage: json["original_language"],
        nextEpisodeToAir: json["next_episode_to_air"],
        numberOfSeasons: json["number_of_seasons"] ?? 0,
        numberOfEpisodes: json["number_of_episodes"] ?? 0,
        lastAirDate: DateTime.parse(json["last_air_date"]),
        firstAirDate: DateTime.parse(json["first_air_date"]),
        // createdBy: List<dynamic>.from(json["created_by"].map((x) => x)),
        episodeRunTime: List<int>.from(json["episode_run_time"].map((x) => x)),
        languages: List<String>.from(json["languages"].map((x) => x)),
        lastEpisodeToAir: Episode.fromJson(json["last_episode_to_air"]),
        originCountry: List<String>.from(json["origin_country"].map((x) => x)),
        seasons:
            List<Season>.from(json["seasons"].map((x) => Season.fromJson(x))),
      );

  bool boolPosterPath() {
    if (posterPath == null) {
      return false;
    } else {
      return true;
    }
  }

  getPosterPath() {
    if (posterPath == null) {
      return 'assets/images/no-image.jpg';
    }
    return 'https://image.tmdb.org/t/p/w500/$posterPath';
  }

  getBackgroungPath() {
    if (backdropPath == null) {
      return 'assets/images/loading.gif';
    }
    return 'https://image.tmdb.org/t/p/w500/$backdropPath';
  }
}

class EpisodesList {
  List<Episode> items = [];
  EpisodesList.fromJsonList(List<dynamic> jsonEpisodesList) {
    for (var element in jsonEpisodesList) {
      final episode = Episode.fromJson(element);
      items.add(episode);
    }
  }
}

class Episode {
  Episode({
    this.id,
    this.name,
    this.airDate,
    this.overview,
    this.stillPath,
    this.voteCount,
    this.voteAverage,
    this.seasonNumber,
    this.episodeNumber,
    this.productionCode,
  });

  int? id;
  String? name;
  int? voteCount;
  String? overview;
  DateTime? airDate;
  String? stillPath;
  int? seasonNumber;
  int? episodeNumber;
  double? voteAverage;
  String? productionCode;

  factory Episode.fromJson(Map<String, dynamic> json) => Episode(
        id: json["id"] ?? 0,
        name: json["name"] ?? '',
        overview: json["overview"] ?? '',
        voteCount: json["vote_count"],
        stillPath: json["still_path"] ,
        seasonNumber: json["season_number"] ?? 0,
        voteAverage: json["vote_average"] / 1,
        episodeNumber: json["episode_number"] ?? 0,
        productionCode: json["production_code"] ?? '',
        airDate: DateTime.parse(json["air_date"]),
      );

  bool boolStillPath() {
    if (stillPath == null) {
      return false;
    } else {
      return true;
    }
  }

  getStillPath() {
    if (stillPath == null) {
      return 'assets/images/no-image.jpg';
    }
    return 'https://image.tmdb.org/t/p/w500/$stillPath';
  }
}

class Season {
  Season({
    this.id,
    this.name,
    // this.airDate,
    this.overview,
    this.posterPath,
    this.seasonNumber,
    this.episodeCount,
  });

  int? id;
  String? name;
  String? overview;
  // DateTime? airDate;
  int? seasonNumber;
  int? episodeCount;
  String? posterPath;

  factory Season.fromJson(Map<String, dynamic> json) => Season(
        id: json["id"] ?? 0,
        name: json["name"] ?? '',
        overview: json["overview"] ?? '',
        posterPath: json["poster_path"],
        episodeCount: json["episode_count"] ?? 0,
        seasonNumber: json["season_number"] ?? 0,
        // airDate: DateTime.parse(json["air_date"]) == null? DateTime.parse('0000-00-00'):DateTime.now()
      );

  bool boolPosterPath() {
    if (posterPath == null) {
      return false;
    } else {
      return true;
    }
  }

  getPosterPath() {
    if (posterPath == null) {
      return 'assets/images/no-image.jpg';
    }
    return 'https://image.tmdb.org/t/p/w500/$posterPath';
  }
}
