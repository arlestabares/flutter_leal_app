// import 'package:flutter_leal_app/core/app/data/repositories/repository.dart';
import 'package:flutter_leal_app/features/provider/popular/data/datasources/remotedatasource/popular_remote_datasource.dart';
import 'package:flutter_leal_app/features/provider/popular/data/repositories/popular_movie_repository.dart';
import 'package:flutter_leal_app/features/provider/recent/data/datasources/remotedatasource/recent_remote_datasource.dart';
import 'package:flutter_leal_app/features/provider/recent/data/repositories/recent_repository.dart';
import 'package:flutter_leal_app/features/provider/recommendations/data/datasources/remotedatasource/recommendations_remote_datasource.dart';
import 'package:flutter_leal_app/features/provider/recommendations/data/repositories/recommendations_repository.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

// import '../../../features/provider/popular/data/repositories/popular_movie_repository.dart';

final sl = GetIt.instance;

Future<void> init() async {
  //registro del Repositorio.
  sl.registerLazySingleton(() => PopularMovieRepositoryImpl(sl()));
  sl.registerLazySingleton(() => RecommendationRepositoryImpl(sl()));
  sl.registerLazySingleton(() => RecentRepositoryImpl(sl()));
  
  //registro DataSources
  sl.registerLazySingleton(() => RecommendationsDataSourceImpl());
  sl.registerLazySingleton(() => RecentRemoteDataSourceImpl());
  sl.registerLazySingleton(() => PopularMovieDataSourceImpl());
  // Dependencias Externas.
  sl.registerLazySingleton(() => http.Client());
}
