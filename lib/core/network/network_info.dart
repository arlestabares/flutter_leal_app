import 'dart:async';
import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:rxdart/subjects.dart';

enum ConnectionStatus {
  onLine,
  ofLine,
}

class CheckInternetConnection {
  final Connectivity _connectivity = Connectivity();
  //controller que emitira estados dependiendo de la conexin
  final _controller = BehaviorSubject.seeded(ConnectionStatus.onLine);
  StreamSubscription? _connectionSubscription;
  CheckInternetConnection() {
    _checkInternetConnection();
  }

  Stream<ConnectionStatus> internetStatus() {
    _connectionSubscription ??= _connectivity.onConnectivityChanged.listen(
      (event) => _checkInternetConnection(),
    );
    return _controller.stream;
  }

  Future<void> _checkInternetConnection() async {
    try {
      Future.delayed(const Duration(seconds: 3));
      final result = await InternetAddress.lookup('google.com',
          type: InternetAddressType.any);
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        _controller.sink.add(ConnectionStatus.onLine);
      } else {
        _controller.sink.add(ConnectionStatus.ofLine);
      }
    } on SocketException catch (_) {
      _controller.sink.add(ConnectionStatus.ofLine);
    }
  }

  Future<void> close() async {
    await _connectionSubscription!.cancel();
    await _controller.close();
  }
}
