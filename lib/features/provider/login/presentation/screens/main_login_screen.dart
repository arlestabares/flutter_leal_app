import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/home/presentation/pages/bottom_navigation_bar_page.dart';
import 'package:flutter_leal_app/features/provider/login/presentation/widgets/close_row_widget.dart';
import 'package:flutter_leal_app/features/provider/login/presentation/widgets/text_form_field_widget.dart';
import 'package:flutter_leal_app/features/provider/login/presentation/widgets/welcome_text_widget.dart';
import 'package:flutter_leal_app/utils/constants.dart';

import '../../../widgets/widgets.dart';

class MainLoginScreen extends StatefulWidget {
  const MainLoginScreen({Key? key}) : super(key: key);

  @override
  State<MainLoginScreen> createState() => _MainLoginScreenState();
}

class _MainLoginScreenState extends State<MainLoginScreen> {
  //
  String inputString = '';

  bool showButtons = true;

  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  @override
  void dispose() {
    _nameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Stack(
      children: [
        Container(
          decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/images/the-witcher2.png'),
                fit: BoxFit.cover),
          ),
        ),
        Center(
          child: Column(
            children: <Widget>[
              const SizedBox(height: 50.0),
              const WelcomeTextWidget(text: 'Welcome!'),
              const Spacer(flex: 3),
              showButtons
                  ? ElevatedButtonWidget(
                      child: const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 26.0),
                        child: Text(
                          'Sign up',
                          style: TextStyle(fontSize: 18.0),
                        ),
                      ),
                      onPressed: () {},
                    )
                  : const SizedBox.shrink(),
              const SizedBox(height: 12.0),
              showButtons
                  ? ElevatedButtonWidget(
                      child: const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 32.0),
                        child: Text(
                          'Log in',
                          style: TextStyle(fontSize: 18.0),
                        ),
                      ),
                      primary: kBackgroundColorButton,
                      onPressed: () {
                        setState(() {
                          showButtons = false;
                        });
                        showModalBottomSheetWidget(context, size);
                      },
                    )
                  : const SizedBox.shrink(),
              const SizedBox(height: 30.0),
              showButtons
                  ? TextButtonWidget(
                      child: const Text(
                        'Forgot password?',
                        style: TextStyle(
                          fontSize: 16.0,
                          color: kTextColorWhite,
                        ),
                      ),
                      onPressed: () {},
                    )
                  : const SizedBox.shrink(),
              const Spacer(flex: 5),
            ],
          ),
        ),
      ],
    );
  }

  Future<dynamic> showModalBottomSheetWidget(BuildContext context, Size size) {
    return showModalBottomSheet(
      isScrollControlled: true,
      barrierColor: Colors.transparent,
      backgroundColor: Colors.black.withOpacity(0.9),
      enableDrag: false,
      isDismissible: false,
      context: context,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(30.0),
        ),
      ),
      builder: (_) {
        return Container(
          height: size.height * 0.6,
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
          color: kTextColorWhite.withOpacity(0.4),
          child: Column(
            children: [
              CloseRowWidget(
                onPressed: () {
                  setState(
                    () {
                      showButtons = true;
                      _passwordController.clear();
                      _nameController.clear();
                    },
                  );
                  Navigator.pop(context);
                },
              ),
              const SizedBox(height: 20.0),
              TextFormFieldWidget(
                hintText: 'Name',
                keyboardType: TextInputType.text,
                nameController: _nameController,
                onChange: (value) {},
              ),
              const SizedBox(height: 36.0),
              TextFormFieldWidget(
                keyboardType: TextInputType.text,
                nameController: _passwordController,
                hintText: 'password',
                fillColor: kTextColorWhite,
                onChange: (value) {},
              ),
              SizedBox(height: size.height * 0.15),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButtonWidget(
                    child: const Padding(
                      padding: EdgeInsets.symmetric(horizontal: 32.0),
                      child: Text(
                        'Login',
                        style: TextStyle(fontSize: 18.0),
                      ),
                    ),
                    primary: kBackgroundColorButton,
                    onPressed: () async {
                      // if (_nameController.text == 'maria' &&
                      //         _passwordController.text == 'password' ||
                      //     _nameController.text == 'pedro' &&
                      //         _passwordController.text == '123456') {
                      // await Provider.of<PopularProvider>(context, listen: false)
                      //     .getPopulares();
                      // await Provider.of<RecommendedMoviesProvider>(context,
                      //         listen: false)
                      //     .getRecommendations();
                      Navigator.pushAndRemoveUntil(
                          context,
                          MaterialPageRoute(
                            builder: (_) => const BottomNavigationBarPage(),
                          ),
                          (route) => false);
                      // }
                    },
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}
