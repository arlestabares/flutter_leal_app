import 'package:flutter/material.dart';

class LoginProvider with ChangeNotifier {
  //
  bool validateNameAndPassword(String name, String password) {
    if (name.isEmpty || password.isEmpty) {
      notifyListeners();
      return false;
    } else {
      notifyListeners();
      return true;
    }
  }
}
