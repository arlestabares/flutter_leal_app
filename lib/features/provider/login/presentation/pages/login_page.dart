import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/login/presentation/screens/main_login_screen.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: MainLoginScreen(),
    );
  }
}
