import 'package:flutter/material.dart';

class TextFormFieldWidget extends StatelessWidget {
  final String hintText;
  final Color? fillColor;
  final TextInputType? keyboardType;
  final Function(String value) onChange;
  final TextEditingController _nameController;
  const TextFormFieldWidget({
    Key? key,
    this.fillColor,
    this.keyboardType,
    required this.hintText,
    required TextEditingController nameController,
    required this.onChange,
  })  : _nameController = nameController,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: keyboardType,
      controller: _nameController,
      decoration: InputDecoration(
        hintText: hintText,
        fillColor: fillColor,
      ),
      onChanged: onChange,
    );
  }
}
