import 'package:flutter/material.dart';
import 'package:flutter_leal_app/utils/constants.dart';

class WelcomeTextWidget extends StatelessWidget {
  final String text;
  const WelcomeTextWidget({
    Key? key,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: const TextStyle(
        fontSize: 29.0,
        fontWeight: FontWeight.bold,
        color: kTextColor,
      ),
    );
  }
}
