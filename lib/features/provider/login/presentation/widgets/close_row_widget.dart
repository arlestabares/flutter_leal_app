import 'package:flutter/material.dart';
import 'package:flutter_leal_app/utils/constants.dart';

class CloseRowWidget extends StatelessWidget {
  final Function() onPressed;
  const CloseRowWidget({
    Key? key,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        IconButton(
          icon: const Icon(
            Icons.close,
            color: kTextColorWhite,
            size: 35.0,
          ),
          onPressed: onPressed,
        )
      ],
    );
  }
}
