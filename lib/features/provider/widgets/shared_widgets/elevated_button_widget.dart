import 'package:flutter/material.dart';
import 'package:flutter_leal_app/utils/constants.dart';

class ElevatedButtonWidget extends StatelessWidget {
  final Widget child;
  final ButtonStyle? style;
  final Color? primary;
  final VoidCallback onPressed;
  final EdgeInsetsGeometry? padding;
  const ElevatedButtonWidget({
    Key? key,
    this.style,
    required this.child,
    required this.onPressed,
    this.padding,
    this.primary = kPrimaryColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        onPrimary: Colors.black87,
        primary: primary ?? kPrimaryColor,
        padding: padding,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(26.0)),
        ),
      ),
      child: child,
      onPressed: onPressed,
    );
  }
}
