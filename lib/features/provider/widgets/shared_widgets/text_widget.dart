import 'package:flutter/material.dart';

class TextWidget extends StatelessWidget {
  final String text;
  final double? fontSize;
  final TextOverflow? overflow;
  final int? maxLines;
  final TextAlign? textAlign;

  final TextStyle? style;
  const TextWidget({
    Key? key,
    required this.text,
    this.fontSize,
    this.style,
    this.overflow,
    this.maxLines,
    this.textAlign,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      maxLines: maxLines,
      style: style,
      overflow: overflow,
      textAlign: textAlign,
    );
  }
}
