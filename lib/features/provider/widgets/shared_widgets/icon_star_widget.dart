import 'package:flutter/material.dart';
import 'package:flutter_leal_app/utils/constants.dart';

class IconStarWidget extends StatelessWidget {
  final IconData icon;
  final Color? color;
  final double? size;
  const IconStarWidget({
    Key? key,
    required this.icon,
    this.color,
    this.size,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Icon(
      icon,
      color: color ?? kTextColor,
      size: size ?? 15.0,
    );
  }
}