import 'package:flutter/material.dart';

import '../widgets.dart';

class RowIconsStarWidget extends StatelessWidget {
  const RowIconsStarWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: const <Widget>[
        IconStarWidget(icon: Icons.star),
        IconStarWidget(icon: Icons.star),
        IconStarWidget(icon: Icons.star),
        IconStarWidget(icon: Icons.star),
        IconStarWidget(icon: Icons.star_border),
      ],
    );
  }
}
