import 'package:flutter/material.dart';

class TextButtonWidget extends StatelessWidget {
  final Function() onPressed;
  final Widget child;
  const TextButtonWidget({
    Key? key,
    required this.child,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextButton(onPressed: onPressed, child: child);
  }
}
