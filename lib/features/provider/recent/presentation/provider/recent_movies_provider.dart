import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/recent/data/models/movie_airing_to_day.dart';

import '../../../../../core/app/data/models/movie_details.dart';
import '../../../../../core/app/data/repositories/repository.dart';

class RecentMoviesProvider with ChangeNotifier {
  final GlobalRepository globalRepository;

  List<MovieAiringToDay> _recentMoviesListProvider = [];
  List<MovieAiringToDay> get recentMoviesListProvider =>
      _recentMoviesListProvider;
  List<Episode> _seasonsListProvider = [];
  List<Episode> get getSeasonsListProvider => _seasonsListProvider;
  MovieDetailsModel _movieDetailsByIdProvider = MovieDetailsModel();
  MovieDetailsModel get movieDetailsByIdProvider => _movieDetailsByIdProvider;
  //
  RecentMoviesProvider(this.globalRepository);
  //
  final _recentStreamController =
      StreamController<List<MovieAiringToDay>>.broadcast();

  Function(List<MovieAiringToDay>) get getRecentSink =>
      _recentStreamController.sink.add;

  Stream<List<MovieAiringToDay>> get getRecentStream =>
      _recentStreamController.stream;

  void disposeStreams() {
    _recentStreamController.close();
  }

  void getSeasonByNumberSeason() {
    // for (var element in _seasonsListProvider) {
    //   //  final season = element.
    // }
    getSeasonsListProvider;
  }

  Future<List<MovieAiringToDay>> getMovies() async {
    List<MovieAiringToDay> response = [];
    _recentMoviesListProvider = await globalRepository.getMovie();
    getRecentSink(_recentMoviesListProvider);

    notifyListeners();
    return response;
  }

  Future<MovieDetailsModel> getMovieDetailsById(int id) async {
    _movieDetailsByIdProvider = await globalRepository.getDetailsById(id);
    int seasonNumber = 0;
    for (var element in _movieDetailsByIdProvider.seasons!) {
      element.seasonNumber = _movieDetailsByIdProvider.numberOfSeasons;
      seasonNumber = element.seasonNumber!;
    }
    _seasonsListProvider = await getSeasons(id, seasonNumber);
    // _episodesProvider = await getEpisode(
    //     id, seasonNumber, _movieDetailsById.lastEpisodeToAir!.episodeNumber!);

    notifyListeners();
    return _movieDetailsByIdProvider;
  }

  Future<List<Episode>> getSeasons(int id, int seasonNumber) async {
    return await globalRepository.getSeasons(id, seasonNumber);
  }

  Future<Episode> getEpisode(
      int id, int seasonNumber, int episodeNumber) async {
    return await globalRepository.getEpisode(id, seasonNumber, episodeNumber);
  }
}
