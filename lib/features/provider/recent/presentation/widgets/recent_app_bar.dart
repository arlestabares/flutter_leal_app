import 'package:flutter/material.dart';
import 'package:flutter_leal_app/utils/constants.dart';

import '../../../widgets/widgets.dart';

AppBar recentAppBar() {
  return AppBar(
    title: const Text(
      'Recent',
      style: TextStyle(
        color: kTextColor,
      ),
    ),
    actions: [
      Padding(
        padding: const EdgeInsets.only(right: 16.0),
        child: IconButtonWidget(
          icon: const Icon(
            Icons.settings_outlined,
            color: kTextColor,
            size: 29.0,
          ),
          onPressed: () {},
        ),
      )
    ],
  );
}
