import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/recent/presentation/provider/recent_movies_provider.dart';
import 'package:flutter_leal_app/utils/constants.dart';
import 'package:provider/provider.dart';

import '../../../../widgets/widgets.dart';
import '../../../data/models/movie_airing_to_day.dart';

class RecentHeroList extends StatelessWidget {
  final MovieAiringToDay recentMovie;
  const RecentHeroList({
    Key? key,
    required this.recentMovie,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      height: size.height * 0.76,
      padding: const EdgeInsets.only(top: 21.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(21.0),
            child: SizedBox(
              child: Image(
                fit: BoxFit.cover,
                image: NetworkImage(recentMovie.getPosterPathRecent()),
                height: size.height * 0.5,
                width: size.width * 0.75,
              ),
            ),
          ),
          const SizedBox(height: 10.0),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              TextWidget(
                text: recentMovie.name!,
                style: Theme.of(context).textTheme.titleLarge!.copyWith(
                    color: kTextColorWhite,
                    fontSize: 26.0,
                    fontWeight: FontWeight.w700),
              ),
              const SizedBox(height: 10.0),
              TextWidget(
                text: recentMovie.name!,
                style: const TextStyle(color: kTextColor),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  const Padding(
                    padding: EdgeInsets.symmetric(vertical: 2.0),
                    child: Text(
                      "Go to view.....",
                      textAlign: TextAlign.right,
                      style: TextStyle(fontSize: 21.0, color: kPrimaryColor),
                    ),
                  ),
                  IconButton(
                    onPressed: () async {
                      await Provider.of<RecentMoviesProvider>(context,
                              listen: false)
                          .getMovieDetailsById(recentMovie.id!);
                      Navigator.pushNamed(context, 'last_episode_to_air_page',
                          arguments: recentMovie);
                    },
                    icon: const Icon(
                      Icons.arrow_forward_ios,
                      color: kPrimaryColor,
                    ),
                  )
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
