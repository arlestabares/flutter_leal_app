import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/home/presentation/widgets/internet_connection/internet_connection_widget.dart';
import 'package:flutter_leal_app/features/provider/recent/data/models/movie_airing_to_day.dart';
import 'package:flutter_leal_app/features/provider/recent/presentation/provider/recent_movies_provider.dart';
import 'package:flutter_leal_app/features/provider/recent/presentation/screens/recent_hero/recent_hero_movies_list.dart';
import 'package:flutter_leal_app/features/provider/recent/presentation/widgets/widgets.dart';
import 'package:flutter_leal_app/features/provider/widgets/widgets.dart';
import 'package:flutter_leal_app/utils/constants.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

class RecentMoviesPage extends StatefulWidget {
  const RecentMoviesPage({Key? key}) : super(key: key);

  @override
  State<RecentMoviesPage> createState() => _RecentMoviesPageState();
}

class _RecentMoviesPageState extends State<RecentMoviesPage> {
  RefreshController _refreshController = RefreshController();

  @override
  void initState() {
    _refreshController = RefreshController();
    onRefresh();
    super.initState();
  }

  void onRefresh() {
    setState(() {});
    Provider.of<RecentMoviesProvider>(context, listen: false).getMovies();
    _refreshController.refreshCompleted();
  }

  void onLoading() async {}

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    return Scaffold(
      appBar: PreferredSize(
        child: recentAppBar(),
        preferredSize: const Size.fromHeight(50.0),
      ),
      body: SmartRefresher(
        controller: _refreshController,
        onRefresh: onRefresh,
        enablePullDown: true,
        header: const WaterDropHeader(
          completeDuration: Duration(milliseconds: 700),
          waterDropColor: kPrimaryColor,
          complete: Icon(
            Icons.check,
            color: kPrimaryColor,
            size: 31.0,
          ),
        ),
        child: ListView(
          children: [
            const InternetConnectionWidget(),
            Container(color: kColorTransparent, height: 40.0),
            StreamBuilderRecentMovie(size: size),
          ],
        ),
      ),
    );
  }
}

class StreamBuilderRecentMovie extends StatelessWidget {
  const StreamBuilderRecentMovie({
    Key? key,
    required this.size,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    final recent = Provider.of<RecentMoviesProvider>(context);
    return Column(
      children: [
        StreamBuilder(
          stream: recent.getRecentStream,
          builder: (context, AsyncSnapshot<List<MovieAiringToDay>> snapshot) {
            if (snapshot.hasData) {
              return ListViewHeroRecent(
                recent: Provider.of<RecentMoviesProvider>(context)
                    .recentMoviesListProvider,
                size: size,
              );
            } else {
              return Center(
                child: Padding(
                  padding: EdgeInsets.only(top: size.height * 0.2),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      TextWidget(
                        text:
                            'Actualice la pagina o \nverifique conexion a internet',
                        textAlign: TextAlign.center,
                        style: Theme.of(context).textTheme.titleLarge!.copyWith(
                            color: kTextColorWhite,
                            overflow: TextOverflow.ellipsis),
                        maxLines: 2,
                      ),
                      const Center(
                        child: Padding(
                          padding: EdgeInsets.only(
                            top: 50.0,
                          ),
                          child: CircularProgressIndicator(
                            backgroundColor: kPrimaryColor,
                            color: kTextColor,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }
          },
        ),
      ],
    );
  }
}

class ListViewHeroRecent extends StatelessWidget {
  const ListViewHeroRecent({
    Key? key,
    required this.recent,
    required this.size,
  }) : super(key: key);
  final Size size;
  final List<MovieAiringToDay> recent;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: size.height * 0.8,
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: recent.length,
        itemBuilder: (context, index) {
          final recentMovie = recent[index];
          return RecentHeroList(
            recentMovie: recentMovie,
          );
        },
      ),
    );
  }
}
