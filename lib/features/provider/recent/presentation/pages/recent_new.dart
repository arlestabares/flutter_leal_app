import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/favorites/presentation/provider/favorites_provider.dart';
import 'package:flutter_leal_app/features/provider/recent/data/models/movie_airing_to_day.dart';
import 'package:flutter_leal_app/features/provider/recent/presentation/provider/recent_movies_provider.dart';
import 'package:flutter_leal_app/utils/constants.dart';
import 'package:provider/provider.dart';

// import 'package:video_player/video_player.dart';

class DetailsLastEpisodeToAirPage extends StatefulWidget {
  const DetailsLastEpisodeToAirPage({Key? key}) : super(key: key);

  @override
  _DetailsLastEpisodeToAirPageState createState() =>
      _DetailsLastEpisodeToAirPageState();
}

class _DetailsLastEpisodeToAirPageState
    extends State<DetailsLastEpisodeToAirPage> {
  // late VideoPlayerController _controller;
  // late Future<void> _initializeVideoPlayerFuture;

  @override
  void initState() {
    super.initState();
    // _controller = VideoPlayerController.network(
    //   'https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4',
    // );

    // _controller.addListener(() {
    //   setState(() {});
    // });
    // _initializeVideoPlayerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    // _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final MovieAiringToDay movieAiringToday =
        ModalRoute.of(context)!.settings.arguments as MovieAiringToDay;
    // Episode episodes = Episode();
    final recentDetail =
        Provider.of<RecentMoviesProvider>(context, listen: false)
            .getSeasonsListProvider;
    int temp = 0;
    for (var element in recentDetail) {
      if (element.stillPath != null) {
        temp++;
      }
    }
    return Scaffold(
        appBar: AppBar(
          elevation: 0.0,
          centerTitle: false,
          title: Text(
            movieAiringToday.name!,
            style: const TextStyle(
              color: kTextColor,
              fontSize: 18.0,
            ),
          ),
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 16.0),
              child: IconButton(
                onPressed: () {
                  setState(() {});
                },
                icon: Icon(
                  Icons.favorite_border_rounded,
                  color: Provider.of<FavoritesMoviesProvider>(context,
                              listen: false)
                          .favorite
                      ? kPrimaryColor
                      : kTextColor,
                  size: 30.0,
                ),
              ),
            )
          ],
        ),
        body: ListView.builder(
            itemCount: temp,
            itemBuilder: (context, index) {
              final episodes = recentDetail[index];

              return episodes.boolStillPath()
                  ? Container(
                      padding: const EdgeInsets.symmetric(horizontal: 16.0),
                      margin: const EdgeInsets.only(top: 21.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '${episodes.episodeNumber} Episode',
                            style: const TextStyle(
                                fontSize: 31.0, color: kPrimaryColor),
                          ),
                          const SizedBox(height: 12.0),
                          Container(
                            height: 250,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(21.0),
                              image: DecorationImage(
                                fit: BoxFit.cover,
                                image: NetworkImage(episodes.boolStillPath()
                                    ? episodes.getStillPath()
                                    : movieAiringToday.getPosterPathRecent()),
                              ),
                            ),
                          ),
                          const SizedBox(height: 31.0),
                          const Divider(height: 3.0, color: Colors.white),
                          const SizedBox(height: 12.0)
                        ],
                      ),
                    )
                  : Container(
                      padding: const EdgeInsets.only(
                          top: 12.0, left: 12.0, right: 12.0),
                      margin: const EdgeInsets.only(top: 18.0),
                      decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.circular(21.0),
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                              movieAiringToday.getPosterPathRecent()),
                        ),
                      ),
                      child: Text(
                        '$index',
                        style: const TextStyle(
                            fontSize: 31.0, color: kPrimaryColor),
                      ),
                      height: 150.0,
                    );
            })
        // Column(
        //   children: <Widget>[
        //     Padding(
        //       padding: const EdgeInsets.only(left: 16.0),
        //       child: Row(
        //         children: const <Widget>[
        //           Expanded(
        //             child: TextWidget(
        //               text: '{recentMovie.id}',
        //               style: TextStyle(
        //                 color: Color(0xFF8C8C8C),
        //               ),
        //             ),
        //           ),
        //         ],
        //       ),
        //     ),
        // Container(
        //   padding: const EdgeInsets.all(20.0),
        //   child: ClipRRect(
        //     borderRadius: BorderRadius.circular(8.0),
        //     child: AspectRatio(
        //       aspectRatio: 2 / 1.1,
        //       child: Stack(
        //         alignment: Alignment.bottomCenter,
        //         children: <Widget>[
        //           VideoPlayerFutureBuilder(
        //             controller: _controller,
        //             initializeVideoPlayerFuture:
        //                 _initializeVideoPlayerFuture,
        //           ),
        //           ClosedCaption(text: _controller.value.caption.text),
        //           _ControlsOverlay(controller: _controller),
        //           VideoProgressIndicator(_controller, allowScrubbing: true)
        //         ],
        //       ),
        //     ),
        //   ),
        // ),

        );
  }
}

// class VideoPlayerFutureBuilder extends StatelessWidget {
//   const VideoPlayerFutureBuilder({
//     Key? key,
//     required Future<void> initializeVideoPlayerFuture,
//     required VideoPlayerController controller,
//   })  : _initializeVideoPlayerFuture = initializeVideoPlayerFuture,
//         _controller = controller,
//         super(key: key);

//   final Future<void> _initializeVideoPlayerFuture;
//   final VideoPlayerController _controller;

//   @override
//   Widget build(BuildContext context) {
//     return FutureBuilder(
//       future: _initializeVideoPlayerFuture,
//       builder: (context, snapshot) {
//         if (snapshot.connectionState == ConnectionState.done) {
//           return VideoPlayer(_controller);
//         } else {
//           return const Center(
//             child: CircularProgressIndicator(
//               color: kPrimaryColor,
//               strokeWidth: 5.0,
//             ),
//           );
//         }
//       },
//     );
//   }
// }

// class _ControlsOverlay extends StatelessWidget {
//   const _ControlsOverlay({Key? key, required this.controller})
//       : super(key: key);

//   final VideoPlayerController controller;

//   @override
//   Widget build(BuildContext context) {
//     return Stack(
//       children: <Widget>[
//         AnimatedSwitcher(
//           duration: const Duration(milliseconds: 50),
//           child: controller.value.isPlaying
//               ? const SizedBox.shrink()
//               : Container(
//                   color: Colors.black26,
//                   child: const Center(
//                     child: CircleAvatar(
//                       backgroundColor: kPrimaryColor,
//                       radius: 26.0,
//                       child: Icon(
//                         Icons.play_arrow,
//                         color: kBackgroundColor,
//                         size: 30.0,
//                         semanticLabel: 'Play',
//                       ),
//                     ),
//                   ),
//                 ),
//         ),
//         GestureDetector(
//           onTap: () {
//             controller.value.isPlaying ? controller.pause() : controller.play();
//           },
//         ),
//       ],
//     );
//   }
// }
