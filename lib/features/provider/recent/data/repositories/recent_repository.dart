import 'dart:async';

import 'package:flutter_leal_app/core/app/data/models/models.dart';
import 'package:flutter_leal_app/features/provider/recent/data/models/movie_airing_to_day.dart';

import '../../../../../core/app/data/datasource/remotedatasource/remote_datasource.dart';
import '../../../../../core/app/data/repositories/repository.dart';

class RecentRepositoryImpl implements GlobalRepository {
  final GlobalRemoteDataSource globalRemoteDataSource;

  RecentRepositoryImpl(this.globalRemoteDataSource);

  @override
  Future<List<MovieAiringToDay>> getMovie() async {
    return await globalRemoteDataSource.getMovie();
  }

  @override
  Future<MovieDetailsModel> getDetailsById(int id) async {
    return await globalRemoteDataSource.getDetailsById(id);
  }

  @override
  Future<List<Episode>> getSeasons(int id, int seasonNumber) async {
    return await globalRemoteDataSource.getSeasons(id, seasonNumber);
  }

  @override
  Future<Episode> getEpisode(
      int id, int seasonNumber, int episodeNumber) async {
    return await globalRemoteDataSource.getEpisode(
        id, seasonNumber, episodeNumber);
  }
}
