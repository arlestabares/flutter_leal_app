import 'dart:convert';

import 'package:flutter_leal_app/core/app/data/datasource/remotedatasource/remote_datasource.dart';
import 'package:flutter_leal_app/features/provider/recent/data/models/movie_airing_to_day.dart';

import '../../../../../../core/app/data/models/models.dart';
import '../../../../../../core/app/services/credentials_api.dart';

import 'package:http/http.dart' as http;

class RecentRemoteDataSourceImpl implements GlobalRemoteDataSource {
  int popularCountPage = 1;
  bool cargando = false;

  @override
  Future<List<MovieAiringToDay>> getMovie() async {
    final uri = Uri.https(CredentialsAPI.url, '3/tv/airing_today', {
      'api_key': CredentialsAPI.apiKey,
      'language': CredentialsAPI.language,
      'page': popularCountPage.toString()
    });
    final response = await _processsResponse(uri);
    return response;
  }

  @override
  Future<MovieDetailsModel> getDetailsById(int id) async {
    final uri = Uri.https(CredentialsAPI.url, '3/tv/$id', {
      'api_key': CredentialsAPI.apiKey,
      'language': CredentialsAPI.language,
    });
    final response = await _processsResponseById(uri);
    return response;
  }

  @override
  Future<List<Episode>> getSeasons(int id, int seasonNumber) async {
    final uri = Uri.https(CredentialsAPI.url, '3/tv/$id/season/$seasonNumber', {
      'api_key': CredentialsAPI.apiKey,
      'language': CredentialsAPI.language,
    });
    final response = await _processSeasonsResponse(uri);

    return response;
  }

  @override
  Future<Episode> getEpisode(
      int id, int seasonNumber, int episodeNumber) async {
    final uri = Uri.https(CredentialsAPI.url,
        '3/tv/$id/season/$seasonNumber/episode/$episodeNumber', {
      'api_key': CredentialsAPI.apiKey,
    });
    final response = await _processEpisodesResponse(uri);
    return response;
  }

  Future<List<MovieAiringToDay>> _processsResponse(Uri uri) async {
    try {
      final response = await http.get(uri);
      final decodeData = json.decode(response.body);
      final movies = MovieAiringTodaytList.fromJsonList(decodeData['results']);
      return movies.items;
    } catch (e) {
      return throw Exception('Algo salio mal');
    }
  }

  Future<MovieDetailsModel> _processsResponseById(Uri uri) async {
    try {
      final response = await http.get(uri);
      final decodeData = json.decode(response.body);
      final moviesDetails = MovieDetailsModel.fromJson(decodeData);

      return moviesDetails;
    } catch (e) {
      throw Exception('Algo salio mal $e');
    }
  }

  Future<List<Episode>> _processSeasonsResponse(Uri uri) async {
    try {
      final response = await http.get(uri);
      final decodeData = json.decode(response.body);
      final season = EpisodesList.fromJsonList(decodeData['episodes']);

      return season.items;
    } catch (e) {
      throw Exception('Algo salio mal $e');
    }
  }

  Future<Episode> _processEpisodesResponse(Uri uri) async {
    try {
      final response = await http.get(uri);
      final decodeData = json.decode(response.body);
      final episode = Episode.fromJson(decodeData);

      return episode;
    } catch (e) {
      throw Exception('Algo salio mal $e');
    }
  }
}
