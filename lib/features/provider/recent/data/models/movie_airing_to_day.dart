class MovieAiringTodaytList {
  List<MovieAiringToDay> items = [];
  MovieAiringTodaytList();
  MovieAiringTodaytList.fromJsonList(List<dynamic> jsonList) {
    for (var element in jsonList) {
      final movieAiringToday = MovieAiringToDay.fromJson(element);
      items.add(movieAiringToday);
    }
  }
}

class MovieAiringToDay {
  MovieAiringToDay({
    this.posterPath,
    this.popularity,
    this.id,
    this.backdropPath,
    this.voteAverage,
    this.overview,
    this.firstAirDate,
    this.originCountry,
    this.genreIds,
    this.originalLanguage,
    this.voteCount,
    this.name,
    this.originalName,
  });
  String? uniquedId = '';
  String? posterPath;
  double? popularity;
  int? id;
  String? backdropPath;
  double? voteAverage;
  String? overview;
  DateTime? firstAirDate;
  List<String>? originCountry;
  List<int>? genreIds;
  String? originalLanguage;
  int? voteCount;
  String? name;
  String? originalName;

  factory MovieAiringToDay.fromJson(Map<String, dynamic> json) =>
      MovieAiringToDay(
        posterPath: json["poster_path"],
        popularity: json["popularity"] / 1,
        id: json["id"],
        backdropPath: json["backdrop_path"],
        voteAverage: json["vote_average"] / 1,
        overview: json["overview"],
        firstAirDate: DateTime.parse(json["first_air_date"]),
        originCountry: List<String>.from(json["origin_country"].map((x) => x)),
        genreIds: List<int>.from(json["genre_ids"].map((x) => x)),
        originalLanguage: json["original_language"],
        voteCount: json["vote_count"],
        name: json["name"],
        originalName: json["original_name"],
      );
  getPosterPathRecent() {
    if (posterPath == null) {
      return 'assets/images/no-image.jpg';
    }
    return 'https://image.tmdb.org/t/p/w500/$posterPath';
  }

  getBackDropPathRecent() {
    if (posterPath == null) {
      return 'assets/images/loading.gif';
    }
    return 'https://image.tmdb.org/t/p/w500/$backdropPath';
  }
}
