import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/recommendations/data/models/recommended_movies.dart';

class FavoritesMoviesProvider with ChangeNotifier {
  bool favorite = false;
  List<RecommendedMovie> _favoriteMovieList = [];
  List<RecommendedMovie> get favoriteMovieList => _favoriteMovieList;

  void addFavoriteMovie(RecommendedMovie movie) {
    if (!_favoriteMovieList.contains(movie)) {
      _favoriteMovieList.add(movie);
      favorite = true;
    }
    notifyListeners();
  }

  void addFavoriteMovieForId(int index, RecommendedMovie movie) {
    _favoriteMovieList.insert(index, movie);
    notifyListeners();
  }

  void removeFavoriteseById(int index) {
    _favoriteMovieList.removeWhere((element) => element.id == index);
    notifyListeners();
  }

  void removeFavoritesByMovie(RecommendedMovie movie) {
    _favoriteMovieList.remove(movie);
    favorite = false;
    notifyListeners();
  }

  void removeItemAt(int index) {
    _favoriteMovieList.removeAt(index);
    notifyListeners();
  }

  void removeAllFavorites() {
    _favoriteMovieList = [];
    notifyListeners();
  }
}
