import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/favorites/presentation/screens/favorits_screen.dart';

class FavoritesPage extends StatelessWidget {
  const FavoritesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: FavoritesScreen(),
    );
  }
}
