import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/favorites/presentation/provider/favorites_provider.dart';
import 'package:flutter_leal_app/features/provider/recommendations/data/models/recommended_movies.dart';
import 'package:flutter_leal_app/utils/constants.dart';
import 'package:provider/provider.dart';

import '../../../../../../utils/style_buttons.dart';
import '../../../../widgets/widgets.dart';

class HeroFavoritesCard extends StatelessWidget {
  const HeroFavoritesCard({
    Key? key,
    required this.size,
    required this.movie,
  }) : super(key: key);

  final Size size;
  final RecommendedMovie movie;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.only(bottom: 16.0),
          height: size.height * 0.23,
          child: Row(
            children: [
              HeroWidget(
                tag: Text('${movie.id}'),
                radius: 10.0,
                networkImage: movie.getPosterImg(),
                assetImage: 'assets/images/no-image.jpg',
                fit: BoxFit.cover,
              ),
              const SizedBox(width: 21.0),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Spacer(),
                  TextWidget(
                    text: movie.originalName!,
                    style: Theme.of(context).textTheme.titleSmall!.copyWith(
                        color: kTextColor, fontSize: 18.0),
                  ),
                  const SizedBox(height: 12.0),
                  const RowIconsStarWidget(),
                  const SizedBox(height: 12.0),
                  TextWidget(
                    text: 'IMDb:${movie.voteAverage!}',
                    style: Theme.of(context)
                        .textTheme
                        .subtitle1!
                        .copyWith(color: kTextColor),
                  ),
                  const SizedBox(height: 15.0),
                  Row(
                    children: [
                      ElevatedButtonWidget(
                        child: const TextWidget(
                          text: 'Watch Now',
                          style: TextStyle(fontSize: 14.0),
                        ),
                        style: elevatedButtonStyleYellow,
                        onPressed: () => Navigator.pushNamed(
                          context,
                          'details_recommendations',
                          arguments: movie,
                        ),
                      ),
                      const SizedBox(width: 12.0),
                      IconButtonWidget(
                        icon: Icon(
                          Icons.favorite_rounded,
                          size: 32.0,
                          color:
                              Theme.of(context).primaryColor.withOpacity(0.3),
                        ),
                        onPressed: () {
                          Provider.of<FavoritesMoviesProvider>(context,
                                  listen: false)
                              .removeFavoritesByMovie(movie);
                        },
                      )
                    ],
                  ),
                ],
              ),
            ],
          ),
        ),
        const Divider(height: 3.0, color: kTextColor),
        const SizedBox(
          height: 12.0,
        )
      ],
    );
  }
}
