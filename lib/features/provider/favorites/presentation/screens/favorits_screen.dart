import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/favorites/presentation/provider/favorites_provider.dart';
import 'package:flutter_leal_app/features/provider/favorites/presentation/screens/components/hero_favorites_card.dart';
import 'package:provider/provider.dart';

class FavoritesScreen extends StatelessWidget {
  const FavoritesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final sieze = MediaQuery.of(context).size;
    final favorites = Provider.of<FavoritesMoviesProvider>(context);

    return Container(
      padding: const EdgeInsets.all(16.0),
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: favorites.favoriteMovieList.length,
        itemBuilder: (context, index) {
          return HeroFavoritesCard(
            size: sieze,
            movie: favorites.favoriteMovieList[index],
          );
        },
      ),
    );
  }
}
