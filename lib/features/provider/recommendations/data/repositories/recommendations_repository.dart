import 'dart:async';

import 'package:flutter_leal_app/core/app/data/repositories/repository.dart';

import '../../../../../core/app/data/datasource/remotedatasource/remote_datasource.dart';

class RecommendationRepositoryImpl implements GlobalRepository {
  // final RecommendationsDataSourceImpl recommendationsDataSource;
  final GlobalRemoteDataSource globalRemoteDataSource;

  RecommendationRepositoryImpl(this.globalRemoteDataSource);

  @override
  Future getMovie() async {
    return await globalRemoteDataSource.getMovie();
  }

  @override
  Future getDetailsById(int id) {
    return globalRemoteDataSource.getDetailsById(id);
  }

  @override
  Future getSeasons(int id, int seasonNumber) {
    return globalRemoteDataSource.getSeasons(id, seasonNumber);
  }

  @override
  Future getEpisode(int id, int seasonNumber, int episodeNumber) {
    return globalRemoteDataSource.getEpisode(
        id, seasonNumber, episodeNumber);
  }
}
