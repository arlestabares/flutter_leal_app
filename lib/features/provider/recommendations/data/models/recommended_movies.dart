class RecommendedMoviesList {
  List<RecommendedMovie> items = [];
  RecommendedMoviesList();

  RecommendedMoviesList.fromJsonList(List<dynamic> jsonList) {
    for (var element in jsonList) {
      final popularMovie = RecommendedMovie.fromJson(element);
      items.add(popularMovie);
    }
  }
}

class RecommendedMovie {
  RecommendedMovie({
    this.backdropPath,
    this.firstAirDate,
    this.genreIds,
    this.id,
    this.originalLanguage,
    this.originalName,
    this.overview,
    this.posterPath,
    this.popularity,
    this.name,
    this.voteAverage,
    this.voteCount,
  });
  //Id para la pelicula, necesario para las transformaciones
  String? uniquedId;
  //
  String? backdropPath;
  DateTime? firstAirDate;
  List<int>? genreIds;
  int? id;
  String? originalLanguage;
  String? originalName;
  String? overview;
  String? posterPath;
  double? popularity;
  String? name;
  double? voteAverage;
  int? voteCount;

  factory RecommendedMovie.fromJson(Map<String, dynamic> json) =>
      RecommendedMovie(
        backdropPath: json["backdrop_path"],
        firstAirDate: DateTime.parse(json["first_air_date"]),
        genreIds: List<int>.from(json["genre_ids"].map((x) => x)),
        id: json["id"],
        originalLanguage: json[json["original_language"]],
        originalName: json["original_name"],
        overview: json["overview"],
        posterPath: json["poster_path"],
        popularity: json["popularity"] / 1,
        name: json["name"],
        voteAverage: json["vote_average"] / 1,
        voteCount: json["vote_count"],
      );

  getPosterImg() {
    if (posterPath == null) {
      return 'assets/images/no-image.jpg';
    }
    return 'https://image.tmdb.org/t/p/w500/$posterPath';
  }

  getBackgroungImg() {
    if (posterPath == null) {
      return 'assets/images/loading.gif';
    }
    return 'https://image.tmdb.org/t/p/w500/$backdropPath';
  }
}
