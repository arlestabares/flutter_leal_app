import 'dart:convert';

import 'package:flutter_leal_app/core/app/data/datasource/remotedatasource/remote_datasource.dart';
import 'package:flutter_leal_app/features/provider/recommendations/data/models/recommended_movies.dart';

import '../../../../../../core/app/data/models/models.dart';
import '../../../../../../core/app/services/credentials_api.dart';

import 'package:http/http.dart' as http;

class RecommendationsDataSourceImpl implements GlobalRemoteDataSource {
  bool _recommendationsLoad = false;
  int _recommendationsCountPage = 0;

  @override
  Future getMovie() async {
    if (_recommendationsLoad) return [];
    _recommendationsLoad = true;
    _recommendationsCountPage++;
    final url = Uri.https(CredentialsAPI.url, '3/tv/top_rated', {
      'api_key': CredentialsAPI.apiKey,
      'language': CredentialsAPI.language,
      'page': _recommendationsCountPage.toString()
    });
    final response = await _processsResponse(url);
    _recommendationsLoad = false;

    return response;
  }

  @override
  Future getDetailsById(int id) async {
    final uri = Uri.https(CredentialsAPI.url, '3/tv/$id', {
      'api_key': CredentialsAPI.apiKey,
      'language': CredentialsAPI.language,
    });
    final response = await _processsResponseById(uri);
    return response;
  }

  @override
  Future getSeasons(int id, int seasonNumber) async {
    final uri = Uri.https(CredentialsAPI.url, '3/tv/$id/season/$seasonNumber', {
      'api_key': CredentialsAPI.apiKey,
      'language': CredentialsAPI.language,
    });
    final response = await _processSeasonsResponse(uri);

    return response;
  }

  @override
  Future<Episode> getEpisode(
      int id, int seasonNumber, int episodeNumber) async {
    final uri = Uri.https(CredentialsAPI.url,
        '3/tv/$id/season/$seasonNumber/episode/$episodeNumber', {
      'api_key': CredentialsAPI.apiKey,
      'language': CredentialsAPI.language,
    });
    final response = await _processEpisodesResponse(uri);

    return response;
  }

  Future<List<RecommendedMovie>> _processsResponse(Uri uri) async {
    try {
      final response = await http.get(uri);
      final decodeData = json.decode(response.body);
      final movies = RecommendedMoviesList.fromJsonList(decodeData['results']);
      return movies.items;
    } catch (e) {
      return List.empty();
    }
  }

  Future<MovieDetailsModel> _processsResponseById(Uri uri) async {
    try {
      final response = await http.get(uri);
      final decodeData = json.decode(response.body);
      final moviesDetails = MovieDetailsModel.fromJson(decodeData);

      return moviesDetails;
    } catch (e) {
      throw Exception('Algo salio mal $e');
    }
  }

  Future<List<Episode>> _processSeasonsResponse(Uri uri) async {
    try {
      final response = await http.get(uri);
      final decodeData = json.decode(response.body);
      final season = EpisodesList.fromJsonList(decodeData);

      return season.items;
    } catch (e) {
      throw Exception('Algo salio mal $e');
    }
  }

  Future<Episode> _processEpisodesResponse(Uri uri) async {
    try {
      final response = await http.get(uri);
      final decodeData = json.decode(response.body);
      final episode = Episode.fromJson(decodeData);

      return episode;
    } catch (e) {
      throw Exception('Algo salio mal $e');
    }
  }
}
