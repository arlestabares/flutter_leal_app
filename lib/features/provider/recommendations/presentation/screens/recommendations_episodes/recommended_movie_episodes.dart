import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/favorites/presentation/provider/favorites_provider.dart';
import 'package:flutter_leal_app/features/provider/recommendations/data/models/recommended_movies.dart';
import 'package:flutter_leal_app/utils/constants.dart';
import 'package:provider/provider.dart';
import 'package:video_player/video_player.dart';

import '../../../../widgets/widgets.dart';

class RecommendedMovieEpisodes extends StatefulWidget {
  const RecommendedMovieEpisodes({Key? key}) : super(key: key);

  @override
  _RecommendedMovieEpisodesState createState() =>
      _RecommendedMovieEpisodesState();
}

class _RecommendedMovieEpisodesState extends State<RecommendedMovieEpisodes> {
  late VideoPlayerController _controller;
  late Future<void> _initializeVideoPlayerFuture;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.network(
      'https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4',
    );

    _controller.addListener(() {
      setState(() {});
    });
    _initializeVideoPlayerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // final size = MediaQuery.of(context).size;
    final RecommendedMovie movie =
        ModalRoute.of(context)!.settings.arguments as RecommendedMovie;
    final favorite = Provider.of<FavoritesMoviesProvider>(context);

    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        centerTitle: false,
        title: Text(
          movie.name!,
          style: const TextStyle(
            color: kTextColor,
            fontSize: 18.0,
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 16.0),
            child: IconButton(
              onPressed: () {
                setState(() {});
                if (favorite.favorite) {
                  favorite.removeFavoritesByMovie(movie);
                } else {
                  favorite.addFavoriteMovie(movie);
                  favorite.favorite = false;
                  setState(() {});
                }
              },
              icon: Icon(
                Icons.favorite_border_rounded,
                color: Provider.of<FavoritesMoviesProvider>(context).favorite
                    ? kPrimaryColor
                    : kTextColor,
                size: 30.0,
              ),
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 16.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: TextWidget(
                      text: '${movie.id}',
                      style: const TextStyle(
                        color: Color(0xFF8C8C8C),
                      ),
                    ),
                  ),
                  const TextWidget(
                    text: 'Next',
                    style: TextStyle(color: kTextColor, fontSize: 21.0),
                  ),
                  IconButtonWidget(
                    icon: const Icon(
                      Icons.arrow_forward_ios,
                      color: kTextColorWhite,
                    ),
                    onPressed: () {},
                  )
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(20.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: AspectRatio(
                  aspectRatio: 2 / 1.1,
                  child: Stack(
                    alignment: Alignment.bottomCenter,
                    children: <Widget>[
                      VideoPlayerFutureBuilder(
                        controller: _controller,
                        initializeVideoPlayerFuture:
                            _initializeVideoPlayerFuture,
                      ),
                      ClosedCaption(text: _controller.value.caption.text),
                      _ControlsOverlay(controller: _controller),
                      VideoProgressIndicator(_controller, allowScrubbing: true)
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const SizedBox(height: 12.0),
                  TextWidget(
                    maxLines: 1,
                    text: movie.name!,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context)
                        .textTheme
                        .titleLarge!
                        .copyWith(color: kTextColorWhite, fontSize: 31.0),
                  ),
                  const SizedBox(height: 21.0),
                  Row(
                    children: [
                      TextWidget(
                        text: 'IMDb: ${movie.voteAverage}',
                        style:
                            const TextStyle(color: kTextColor, fontSize: 14.0),
                      ),
                      const SizedBox(width: 6.0),
                      const TextWidget(
                        text: '|',
                        style: TextStyle(fontSize: 12.0, color: kTextColor),
                      ),
                      const SizedBox(width: 8.0),
                      TextWidget(
                        text: '${movie.firstAirDate}',
                        style: const TextStyle(
                          fontSize: 14.0,
                          color: kTextColor,
                        ),
                      ),
                      const SizedBox(width: 8.0),
                      const TextWidget(
                        text: '|',
                        style: TextStyle(fontSize: 12.0, color: kTextColor),
                      ),
                      const SizedBox(width: 8.0),
                      TextWidget(
                        text: '${movie.voteAverage}',
                        style: const TextStyle(
                          fontSize: 14.0,
                          color: kTextColor,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 21.0),
                  const Divider(height: 3.0, color: kTextColor),
                  const SizedBox(height: 21.0),
                  TextWidget(
                    text: movie.originalName!,
                    style: Theme.of(context)
                        .textTheme
                        .bodyLarge!
                        .copyWith(color: kTextColor),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  // FloatingActionButton floatingActionButtonVideoPlayer() {
  //   return FloatingActionButton(
  //     onPressed: () {
  //       setState(
  //         () {
  //           if (_controller.value.isPlaying) {
  //             _controller.pause();
  //           } else {
  //             _controller.play();
  //           }
  //         },
  //       );
  //     },
  //     child: Icon(
  //       _controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
  //     ),
  //   );
  // }
}

class VideoPlayerFutureBuilder extends StatelessWidget {
  const VideoPlayerFutureBuilder({
    Key? key,
    required Future<void> initializeVideoPlayerFuture,
    required VideoPlayerController controller,
  })  : _initializeVideoPlayerFuture = initializeVideoPlayerFuture,
        _controller = controller,
        super(key: key);

  final Future<void> _initializeVideoPlayerFuture;
  final VideoPlayerController _controller;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _initializeVideoPlayerFuture,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return VideoPlayer(_controller);
        } else {
          return const Center(
            child: CircularProgressIndicator(
              color: kPrimaryColor,
              strokeWidth: 5.0,
            ),
          );
        }
      },
    );
  }
}

class _ControlsOverlay extends StatelessWidget {
  const _ControlsOverlay({Key? key, required this.controller})
      : super(key: key);

  final VideoPlayerController controller;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        AnimatedSwitcher(
          duration: const Duration(milliseconds: 50),
          child: controller.value.isPlaying
              ? const SizedBox.shrink()
              : Container(
                  color: Colors.black26,
                  child: const Center(
                    child: CircleAvatar(
                      backgroundColor: kPrimaryColor,
                      radius: 26.0,
                      child: Icon(
                        Icons.play_arrow,
                        color: kBackgroundColor,
                        size: 30.0,
                        semanticLabel: 'Play',
                      ),
                    ),
                  ),
                ),
        ),
        GestureDetector(
          onTap: () {
            controller.value.isPlaying ? controller.pause() : controller.play();
          },
        ),
      ],
    );
  }
}
