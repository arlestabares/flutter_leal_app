import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/recommendations/presentation/provider/recommendations_provider.dart';
import 'package:flutter_leal_app/features/provider/recommendations/presentation/screens/components/stream_builder_recommendations.dart';
import 'package:provider/provider.dart';

class RecommendationsScreen extends StatelessWidget {
  const RecommendationsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final recommendations = Provider.of<RecommendedMoviesProvider>(context,listen: false);
    return StreamBuilderRecommendations(recommendations: recommendations);
  }
}
