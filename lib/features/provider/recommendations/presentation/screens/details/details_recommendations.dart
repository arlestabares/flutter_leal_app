import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/recommendations/data/models/recommended_movies.dart';
import 'package:flutter_leal_app/utils/constants.dart';

import '../../../../widgets/widgets.dart';

class DetailsRecommendationsScreen extends StatelessWidget {
  const DetailsRecommendationsScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final RecommendedMovie movie = ModalRoute.of(context)!.settings.arguments as RecommendedMovie;
    movie.uniquedId = '${movie.name}';
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Hero(
            tag: movie.uniquedId! ,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(12.0),
              child: Image(
                image: NetworkImage(movie.getPosterImg()),
                height: size.height * 0.5,
              ),
            ),
          ),
          const SizedBox(height: 30.0),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              TextWidget(
                text: movie.originalName!,
                style: Theme.of(context).textTheme.titleLarge!.copyWith(
                      color: kTextColorWhite,
                    ),
              ),
              const SizedBox(height: 12.0),
              const RowIconsStarWidget(),
              const SizedBox(height: 12.0),
              TextWidget(
                text: 'IMDb:${movie.voteAverage!}',
                style: const TextStyle(color: kTextColor),
              ),
              const SizedBox(height: 20.0),
              ElevatedButtonWidget(
                child: const TextWidget(
                  text: 'Watch Now',
                  style: TextStyle(fontSize: 16.0),
                ),
                onPressed: () => Navigator.pushNamed(
                    context, 'recommendations_watch_now',
                    arguments: movie),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
