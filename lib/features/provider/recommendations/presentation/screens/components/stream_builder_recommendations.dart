import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/recommendations/presentation/provider/recommendations_provider.dart';
import 'package:flutter_leal_app/features/provider/recommendations/presentation/screens/components/list_view_recommendations.dart';
import 'package:flutter_leal_app/utils/constants.dart';

class StreamBuilderRecommendations extends StatelessWidget {
  const StreamBuilderRecommendations({
    Key? key,
    required this.recommendations,
  }) : super(key: key);

  final RecommendedMoviesProvider recommendations;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: recommendations.getRecommendationsStream,
      builder: (context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.hasData) {
          return ListViewRecommendations(
            movies: recommendations.recommendationsList,
            // nextPage: movsieProvider.getPopulares,
          );
        } else {
          return const Center(
            child: CircularProgressIndicator(
              backgroundColor: kPrimaryColor,
            ),
          );
        }
      },
    );
  }
}
