import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/favorites/presentation/provider/favorites_provider.dart';
import 'package:flutter_leal_app/features/provider/recommendations/data/models/recommended_movies.dart';
import 'package:flutter_leal_app/utils/constants.dart';
import 'package:provider/provider.dart';

import '../../../../widgets/widgets.dart';

class HeroRecommendations extends StatelessWidget {
  final RecommendedMovie movie;
  const HeroRecommendations({
    Key? key,
    required this.movie,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final favorites =
        Provider.of<FavoritesMoviesProvider>(context, listen: false);
    movie.uniquedId = '${movie.name}';
    return Container(
      height: MediaQuery.of(context).size.height * 0.24,
      // width: double.infinity,
      margin: const EdgeInsets.only(top: 15.0, bottom: 10.0),
      child: Row(
        children: [
          GestureDetector(
            onTap: () => Navigator.pushNamed(
              context,
              'details_recommendations',
              arguments: movie,
            ),
            child: HeroWidget(
              tag: movie.uniquedId!,
              radius: 10.0,
              networkImage: movie.getPosterImg(),
              assetImage: 'assets/images/no-image.jpg',
              fit: BoxFit.cover,
            ),
          ),
          const SizedBox(width: 18.0),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Spacer(),
              SizedBox(
                width: 150.0,
                child: TextWidget(
                  maxLines: 2,
                  text: movie.originalName!,
                  style: Theme.of(context)
                      .textTheme
                      .titleSmall!
                          .copyWith(color: kTextColor, fontSize: 18.0,overflow: TextOverflow.ellipsis),
                ),
              ),
              const SizedBox(height: 12.0),
              const RowIconsStarWidget(),
              const SizedBox(height: 12.0),
              TextWidget(
                text: 'IMDb:${movie.voteAverage!}',
                style: Theme.of(context)
                    .textTheme
                    .subtitle1!
                    .copyWith(color: kTextColor),
              ),
              const SizedBox(height: 15.0),
              Row(
                children: [
                  ElevatedButtonWidget(
                    child: const TextWidget(
                      text: 'Watch Now',
                      style: TextStyle(fontSize: 14.0),
                    ),
                    onPressed: () => Navigator.pushNamed(
                        context, 'recommendations_watch_now',
                        arguments: movie),
                  ),
                  const SizedBox(width: 28.0),
                  InkWell(
                    child: IconButtonWidget(
                      icon: const Icon(
                        Icons.favorite_border,
                        size: 38.0,
                        color: kTextColor,
                      ),
                      onPressed: () {
                        favorites.addFavoriteMovie(movie);
                      },
                    ),
                  )
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
