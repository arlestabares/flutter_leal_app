import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/material.dart';

import '../../../../popular/data/models/popular_movie.dart';

class CardSwiper extends StatelessWidget {
  // final Function nextPage;

  final _pageController = PageController(
    initialPage: 1,
    //viewportFraction: 0.3,
  );

  final List<PopularMovie> movies;

  CardSwiper({
    Key? key,
    required this.movies,
    // required this.nextPage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _pageController.addListener(() {
      if (_pageController.position.pixels >=
          _pageController.position.maxScrollExtent - 300) {
        // nextPage();
      }
    });
    final _screenSize = MediaQuery.of(context).size;

    return Container(
      padding: const EdgeInsets.only(top: 2.0),
      child: Swiper(
        //pila de swiperlayout y le especificamos el tamaño a las tarjetas
        layout: SwiperLayout.STACK,
        itemWidth: _screenSize.width * 0.7,
        itemHeight: _screenSize.height * 0.3,
        itemCount: movies.length,
        itemBuilder: (BuildContext context, int index) {
          //uniquedId creado en la Clase movie.model
          movies[index].uniquedId = '${movies[index].id}-tarjeta';

          return Hero(
            tag: movies[index].uniquedId!,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: GestureDetector(
                onTap: () => Navigator.pushNamed(context, 'detalle',
                    arguments: movies[index]),
                child: FadeInImage(
                  image: NetworkImage(movies[index].getPosterImg()),
                  placeholder: const AssetImage('assets/images/no-image.jpg'),
                  fit: BoxFit.cover,
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
