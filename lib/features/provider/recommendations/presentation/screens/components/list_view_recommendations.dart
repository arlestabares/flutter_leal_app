import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/recommendations/data/models/recommended_movies.dart';

import 'hero_recommendations_card.dart';

class ListViewRecommendations extends StatelessWidget {
  final List<RecommendedMovie> movies;

  const ListViewRecommendations({Key? key, required this.movies})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      child: ListView.builder(
        shrinkWrap: true,
        itemCount: movies.length,
        itemBuilder: (context, index) {
          final pelicula = movies[index];
          return HeroRecommendations(
            movie: pelicula,
          );
        },
      ),
    );
  }
}
