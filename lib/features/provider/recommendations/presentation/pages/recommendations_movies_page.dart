import 'package:flutter/material.dart';

import '../screens/details/details_recommendations.dart';

class RecommendationsMoviesPage extends StatelessWidget {
  const RecommendationsMoviesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Recommendations'),
        centerTitle: false,
      ),
      body: const DetailsRecommendationsScreen(),
    );
  }
}
