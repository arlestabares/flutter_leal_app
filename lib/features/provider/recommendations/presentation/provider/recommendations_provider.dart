import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/recommendations/data/models/recommended_movies.dart';

import '../../../../../core/app/data/repositories/repository.dart';

class RecommendedMoviesProvider with ChangeNotifier {
  final GlobalRepository globalRepository;

  List<RecommendedMovie> _recommendationsList = [];
  List<RecommendedMovie> get recommendationsList => _recommendationsList;

  final _recommendationsStreamController =
      StreamController<List<RecommendedMovie>>.broadcast();
  Function(List<RecommendedMovie>) get getRecommendationsSink =>
      _recommendationsStreamController.sink.add;

  Stream<List<RecommendedMovie>> get getRecommendationsStream =>
      _recommendationsStreamController.stream;

  void disposeStreams() {
    _recommendationsStreamController.close();
  }

  RecommendedMoviesProvider(this.globalRepository);

  Future<List<RecommendedMovie>> getRecommendations() async {
    _recommendationsList = await globalRepository.getMovie();
    getRecommendationsSink(_recommendationsList);
    notifyListeners();
    return _recommendationsList;
  }
}
