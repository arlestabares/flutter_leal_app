import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../favorites/presentation/widgets/widgets.dart';
import '../login/presentation/provider/login_provider.dart';
import '../popular/data/datasources/remotedatasource/popular_remote_datasource.dart';
import '../popular/presentation/provider/popular_provier.dart';
import '../recent/data/datasources/remotedatasource/recent_remote_datasource.dart';
import '../recent/data/repositories/recent_repository.dart';
import '../recent/presentation/provider/recent_movies_provider.dart';
import '../recommendations/data/datasources/remotedatasource/recommendations_remote_datasource.dart';
import '../recommendations/data/repositories/recommendations_repository.dart';
import '../recommendations/presentation/provider/recommendations_provider.dart';
import '../search/presentation/provider/search_provider.dart';

class MultiproviderWidget extends StatelessWidget {
  const MultiproviderWidget({Key? key, required this.child}) : super(key: key);
  final Widget child;
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
         ChangeNotifierProvider<FavoritesMoviesProvider>(
          create: (context) => FavoritesMoviesProvider(),
        ),
        ChangeNotifierProvider<LoginProvider>(
          create: (context) => LoginProvider(),
        ),
        ChangeNotifierProvider<PopularProvider>(
          create: (context) => PopularProvider(
              PopularMovieDataSourceImpl()),
        ),
        ChangeNotifierProvider<RecentMoviesProvider>(
          create: (context) => RecentMoviesProvider(
              RecentRepositoryImpl(RecentRemoteDataSourceImpl())),
        ),
        ChangeNotifierProvider<RecommendedMoviesProvider>(
          create: (context) => RecommendedMoviesProvider(
              RecommendationRepositoryImpl(RecommendationsDataSourceImpl())),
        ),
        ChangeNotifierProvider<SearchMoviesProvider>(
          create: (context) => SearchMoviesProvider(),
        ),
      ],
      child: child,
    );
  }
}
