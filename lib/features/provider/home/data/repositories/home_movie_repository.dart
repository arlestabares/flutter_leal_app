import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import '../../../popular/data/models/popular_movie.dart';
import 'package:http/http.dart' as http;

class HomeMoviesRepository {
  //994983fbf90eaefe54618b962f337902
  final String _apiKey = '994983fbf90eaefe54618b962f337902';
  final String _url = 'api.themoviedb.org';
  final String _language = 'en-US';
  int _popularesPageCount = 0;
  bool _cargando = false;

  final List<PopularMovie> _popularesList = [];

  final _popularesStreamController = StreamController<List<PopularMovie>>.broadcast();

  Function(List<PopularMovie>) get getPopularesSink =>
      _popularesStreamController.sink.add;

  Stream<List<PopularMovie>> get getPopularesStream =>
      _popularesStreamController.stream;

  void disposeStream() {
    _popularesStreamController.close();
  }

  Future<List<PopularMovie>> getPopulares() async {
    if (_cargando) return [];
    _cargando = true;
    _popularesPageCount++;
    final uri = Uri.https(_url, '3/tv/popular', {
      'api_key': _apiKey,
      'language': _language,
      'page': _popularesPageCount.toString()
    });
    final response = await _processsResponse(uri);
    _cargando = false;
    _popularesList.addAll(response);
    getPopularesSink(_popularesList);
    return response;
  }

  Future<List<PopularMovie>> getRecommendations() async {
    final url = Uri.https(_url, '3/tv/top_rated', {
      'aapi_key': _apiKey,
      'language': _language,
    });
    return await _processsResponse(url);
  }

  Future<List<PopularMovie>> getDetails(String tvId) async {
    final url = Uri.https(_url, '3/tv/$tvId', {
      'api_key': _apiKey,
      'language': _language,
    });
    return await _processsResponse(url);
  }

  Future getSeasons(String tvId, int seasonNumber) async {
    final url = Uri.https(_url, '3/tv/$tvId/season_number/$seasonNumber', {
      'api_key': _apiKey,
      'language': _language,
    });
    return await _processsResponse(url);
  }

  Future<List<PopularMovie>> _processsResponse(Uri uri) async {
    try {
      final response = await http.get(uri);
      final decodeData = json.decode(response.body);
      final movies = MovieList.fromJsonList(decodeData['results']);
      log('${movies.items}');
      return movies.items;
    } catch (e) {
      return List.empty();
    }
  }
}
