import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/popular/presentation/provider/popular_provier.dart';
import 'package:flutter_leal_app/features/provider/popular/presentation/screens/main_screen_popular_movies.dart';
import 'package:flutter_leal_app/features/provider/recommendations/presentation/screens/main_recommendations_screen.dart';
import 'package:flutter_leal_app/utils/constants.dart';
import 'package:provider/provider.dart';

class MainMoviesScreen extends StatelessWidget {
  const MainMoviesScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // final size = MediaQuery.of(context).size;
    return Container(
      // height: size.height + 180.0,
      padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 12.0),
      child: Column(
        children: <Widget>[
          MainScreenPopularMovies(
            popular: Provider.of<PopularProvider>(context, listen: false),
          ),
          Container(
            width: double.infinity,
            padding: const EdgeInsets.only(top: 10.0, bottom: 25.0),
            child: Text(
              'Recommendations',
              style: Theme.of(context).textTheme.headline5!.copyWith(
                    color: kTextColorWhite,
                  ),
            ),
          ),
          const Expanded(child: RecommendationsScreen()),
        ],
      ),
    );
  }
}
