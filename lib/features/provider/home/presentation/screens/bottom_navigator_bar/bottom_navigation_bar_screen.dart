import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/favorites/presentation/widgets/widgets.dart';
import 'package:flutter_leal_app/features/provider/home/presentation/pages/home_page.dart';
import 'package:flutter_leal_app/features/provider/recent/presentation/pages/recent_movies_page.dart';
import 'package:flutter_leal_app/features/provider/search/presentation/pages/search_page.dart';
import 'package:flutter_leal_app/utils/constants.dart';

class BottomNavigationBarScreen extends StatefulWidget {
  const BottomNavigationBarScreen({Key? key}) : super(key: key);

  @override
  State<BottomNavigationBarScreen> createState() =>
      _BottomNavigationBarScreenState();
}

class _BottomNavigationBarScreenState extends State<BottomNavigationBarScreen> {
  static int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  static const List<Widget> _widgetOptions = <Widget>[
    HomePage(),
    FavoritesPage(),
    RecentMoviesPage(),
    SearchPage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: kBackgroundColor.withOpacity(0.7),
      body: IndexedStack(
        index: _selectedIndex,
        children: _widgetOptions,
      ),
      bottomNavigationBar: buildBottomNavigationBar(),
    );
  }

  BottomNavigationBar buildBottomNavigationBar() {
    return BottomNavigationBar(
      selectedFontSize: 16.0,
      elevation: 10.0,
      backgroundColor: kBackgroundColor,
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home, color: kTextColorWhite),
          label: "Home",
          backgroundColor: Colors.black,
        ),
        BottomNavigationBarItem(
            icon: Icon(Icons.favorite, color: kTextColorWhite),
            label: "Favorites",
            backgroundColor: Colors.black),
        BottomNavigationBarItem(
          icon: Icon(Icons.play_circle_outline_rounded, color: kTextColorWhite),
          label: "Recents",
          backgroundColor: Colors.black,
        ),
        BottomNavigationBarItem(
            icon: Icon(Icons.search, color: kTextColorWhite),
            label: "Search",
            backgroundColor: Colors.black),
      ],
      currentIndex: _selectedIndex,
      onTap: _onItemTapped,
      selectedItemColor: kPrimaryColor,
    );
  }
}
