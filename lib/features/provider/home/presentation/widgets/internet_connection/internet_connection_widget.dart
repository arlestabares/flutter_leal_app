import 'package:flutter/material.dart';
import 'package:flutter_leal_app/core/app/provider/connection_status_change_notifier.dart';
import 'package:flutter_leal_app/core/network/network_info.dart';
import 'package:flutter_leal_app/features/provider/widgets/widgets.dart';
import 'package:provider/provider.dart';

import '../../../../../../core/app/provider/connection_status_change_notifier.dart';

class InternetConnectionWidget extends StatelessWidget {
  const InternetConnectionWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => ConnectionStatusChangeNotifier(),
      child: Consumer<ConnectionStatusChangeNotifier>(
          builder: (context, value, child) {
        return Visibility(
          visible: value.status != ConnectionStatus.onLine,
          child: Center(
            child: Container(
              padding: const EdgeInsets.all(16.0),
              height: 60.0,
              color: const Color(0xFFFFD233),
              child: Row(
                children: const <Widget>[
                  Icon(Icons.wifi_off),
                  SizedBox(width: 21.0),
                  TextWidget(text: 'No internet Connection.')
                ],
              ),
            ),
          ),
        );
      }),
    );
  }
}
