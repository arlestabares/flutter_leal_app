import 'package:flutter/material.dart';

import '../../../login/presentation/pages/login_page.dart';

AppBar appBarHomePage(BuildContext context) {
  return AppBar(
    elevation: 0.0,
    backgroundColor: const Color(0xFF191919),
    actions: [
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: IconButton(
          icon: Icon(
            Icons.settings,
            color: Colors.white.withOpacity(0.5),
            size: 26.0,
          ),
          onPressed: () => Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (_) => const LoginPage()),
              (route) => false),
        ),
      )
    ],
    centerTitle: true,
    title: Text(
      'Home',
      style: TextStyle(fontSize: 23.0, color: Colors.white.withOpacity(0.6)),
    ),
  );
}
