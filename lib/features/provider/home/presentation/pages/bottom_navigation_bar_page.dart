import 'package:flutter/material.dart';

import '../screens/bottom_navigator_bar/bottom_navigation_bar_screen.dart';

class BottomNavigationBarPage extends StatelessWidget {
  const BottomNavigationBarPage({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: BottomNavigationBarScreen(),
    );
  }
}