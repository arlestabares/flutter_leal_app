import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/home/presentation/screens/main_movies_screen.dart';
import 'package:flutter_leal_app/features/provider/popular/presentation/provider/popular_provier.dart';
import 'package:provider/provider.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../recommendations/presentation/provider/recommendations_provider.dart';
import '../widgets/widgets.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  RefreshController _refreshController = RefreshController();
  late ScrollController controller;
  @override
  void initState() {
    _refreshController = RefreshController();
    controller = ScrollController();
    onRefresh();
    super.initState();
  }

  @override
  void dispose() {
    _refreshController.dispose();
    super.dispose();
  }

  void onRefresh() async {
    setState(() {});
    Future.delayed(const Duration(seconds: 5));
    await Provider.of<PopularProvider>(context,listen: false).getPopulares();
    await Provider.of<RecommendedMoviesProvider>(context,listen: false)
        .getRecommendations();
    // await Provider.of<RecentMoviesProvider>(context, listen: false)
    //     .getMovies();

    _refreshController.refreshCompleted();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarHomePage(context),
      body: SmartRefresher(
        controller: _refreshController,
        onRefresh: onRefresh,
        enablePullDown: true,
        header: const WaterDropHeader(
          completeDuration: Duration(milliseconds: 700),
          waterDropColor: Color(0xFFFFD233),
          complete: Icon(
            Icons.check,
            color: Color(0xFFFFD233),
            size: 31.0,
          ),
        ),
        child: getMainMovies(),
      ),
    );
  }

  ListView getMainMovies() {
    return ListView(
      children: const <Widget>[
         InternetConnectionWidget(),
      //   MainScreenPopularMovies(
      //     popular: Provider.of<PopularProvider>(context, listen: false),
      //   ),
      //   Container(
      //     width: double.infinity,
      //     padding: const EdgeInsets.only(top: 10.0, bottom: 25.0),
      //     child: Text(
      //       'Recommendations',
      //       style: Theme.of(context).textTheme.headline5!.copyWith(
      //             color: kTextColorWhite,
      //           ),
      //     ),
      //   ),
      //  const  Expanded(child: RecommendationsScreen())
        MainMoviesScreen(),
      ],
    );
  }
}
