import 'package:flutter/material.dart';

import '../screens/details/popular_movie_details_screen.dart';

class PopularMoviesPage extends StatelessWidget {
  const PopularMoviesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Popular'),
        centerTitle: false,
      ),
      body:  const PopularMoviesDetailsScreen(),
    );
  }
}
