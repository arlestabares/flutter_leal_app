import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/popular/data/datasources/remotedatasource/popular_remote_datasource.dart';
import 'package:flutter_leal_app/features/provider/popular/data/models/popular_movie.dart';

import '../../../../../core/app/data/models/movie_details.dart';
// import '../../../../../core/app/data/repositories/repository.dart';

class PopularProvider with ChangeNotifier {
  final PopularMovieDataSourceImpl globalRepository;

  PopularProvider(this.globalRepository);

  List<PopularMovie> popularesList = [];
  List<PopularMovie> get getPopularesList => popularesList;
  Episode _episodesProvider = Episode();
  Episode get getEpisodesProvider => _episodesProvider;
  MovieDetailsModel _movieDetailsModel = MovieDetailsModel();
  MovieDetailsModel get getMovieDetailsModel => _movieDetailsModel;
  final _popularesStreamController = StreamController<List<PopularMovie>>.broadcast();

  Function(List<PopularMovie>) get getPopularesSink =>
      _popularesStreamController.sink.add;
  Stream<List<PopularMovie>> get getPopularesStream =>
      _popularesStreamController.stream;

  void disposeStreams() {
    _popularesStreamController.close();
  }

  Future<List<PopularMovie>> getPopulares() async {
    List<PopularMovie> response = [];
    response = await globalRepository.getMovie();
    popularesList.addAll(response);
    getPopularesSink(popularesList);
    notifyListeners();
    return popularesList;
  }

  Future<MovieDetailsModel> getMovieDetailsById(int id) async {
    _movieDetailsModel = await globalRepository.getDetailsById(id);
    int seasonNumber = 0;
    for (var element in _movieDetailsModel.seasons!) {
      seasonNumber = element.seasonNumber!;
    }
    _episodesProvider = await getEpisode(
        id, seasonNumber, _movieDetailsModel.lastEpisodeToAir!.episodeNumber!);

    notifyListeners();
    return _movieDetailsModel;
  }

  Future<Episode> getEpisode(
      int id, int seasonNumber, int episodeNumber) async {
    Episode response;
    response =
        await globalRepository.getEpisode(id, seasonNumber, episodeNumber);
    notifyListeners();
    return response;
  }
}
