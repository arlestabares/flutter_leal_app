import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/popular/data/models/popular_movie.dart';
import 'package:flutter_leal_app/features/provider/popular/presentation/provider/popular_provier.dart';
import 'package:flutter_leal_app/utils/constants.dart';
import 'package:provider/provider.dart';

import '../../../../widgets/widgets.dart';

class PopularMoviesDetailsScreen extends StatelessWidget {
  const PopularMoviesDetailsScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final PopularMovie movie = ModalRoute.of(context)!.settings.arguments as PopularMovie;
    movie.uniquedId = '${movie.name}';
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: Column(
        children: [
          Hero(
            tag: movie.uniquedId!,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(41.0),
              child: Image(
                image: NetworkImage(movie.getPosterImg()),
                height: size.height * 0.5,
              ),
            ),
          ),
          const SizedBox(height: 30.0),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              TextWidget(
                maxLines: 1,
                text: movie.originalName!,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontSize: 41.0,
                  color: kTextColorWhite,
                  overflow: TextOverflow.ellipsis
                ),
              ),
              const SizedBox(height: 12.0),
              const RowIconsStarWidget(),
              const SizedBox(height: 12.0),
              TextWidget(
                text: 'IMDb:${movie.voteAverage!}',
                style: const TextStyle(color: kTextColor),
              ),
              const SizedBox(height: 20.0),
              ElevatedButtonWidget(
                child: const TextWidget(
                  text: 'Watch Now',
                  style: TextStyle(fontSize: 16.0),
                ),
                onPressed: () {
                  
                  Provider.of<PopularProvider>(context, listen: false)
                      .getMovieDetailsById(movie.id!);
                  Navigator.pushNamed(
                    context,
                    'popular_watch_now',
                    arguments: movie,
                  );
                },
              )
            ],
          ),
        ],
      ),
    );
  }
}
