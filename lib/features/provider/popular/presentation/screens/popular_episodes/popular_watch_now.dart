import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/popular/data/models/popular_movie.dart';
import 'package:flutter_leal_app/features/provider/popular/presentation/provider/popular_provier.dart';
import 'package:flutter_leal_app/utils/constants.dart';
import 'package:provider/provider.dart';
import 'package:video_player/video_player.dart';

import '../../../../widgets/widgets.dart';

class PopularEpisodes extends StatefulWidget {
  const PopularEpisodes({Key? key}) : super(key: key);

  @override
  _PopularEpisodesState createState() => _PopularEpisodesState();
}

class _PopularEpisodesState extends State<PopularEpisodes> {
  late VideoPlayerController _controller;
  late Future<void> _initializeVideoPlayerFuture;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.network(
      // 'https://www.bbc.co.uk/programmes/b045fz8r',
      "https://www.netflix.com/title/81261170",
    );

    _controller.addListener(() {
      setState(() {});
    });
    _initializeVideoPlayerFuture = _controller.initialize();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final PopularMovie movie = ModalRoute.of(context)!.settings.arguments as PopularMovie;
    final episode = Provider.of<PopularProvider>(context).getEpisodesProvider;
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        centerTitle: false,
        title: const Text(
          'episode.name',
          style: TextStyle(
            color: kTextColor,
            fontSize: 18.0,
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 16.0),
            child: IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.favorite_border_rounded,
                color: kTextColor,
                size: 30.0,
              ),
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 16.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: TextWidget(
                      text: '${episode.id}',
                      style: const TextStyle(
                        color: Color(0xFF8C8C8C),
                      ),
                    ),
                  ),
                  const TextWidget(
                    text: 'Next',
                    style: TextStyle(color: kTextColor, fontSize: 21.0),
                  ),
                  IconButtonWidget(
                    icon: const Icon(
                      Icons.arrow_forward_ios,
                      color: kTextColorWhite,
                    ),
                    onPressed: () async {
                      // Provider.of<FavoritesMoviesProvider>(context,listen:false).addFavoriteMovie(movie);
                    },
                  )
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(20.0),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(8.0),
                child: AspectRatio(
                  aspectRatio: 2 / 1.1,
                  child: Stack(
                    alignment: Alignment.bottomCenter,
                    children: <Widget>[
                      VideoPlayerFutureBuilder(
                        controller: _controller,
                        initializeVideoPlayerFuture:
                            _initializeVideoPlayerFuture,
                      ),
                      ClosedCaption(text: _controller.value.caption.text),
                      _ControlsOverlay(controller: _controller),
                      VideoProgressIndicator(_controller, allowScrubbing: true)
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const SizedBox(height: 12.0),
                  TextWidget(
                    maxLines: 1,
                    text: episode.name!,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context)
                        .textTheme
                        .titleLarge!
                        .copyWith(color: kTextColorWhite, fontSize: 31.0),
                  ),
                  const SizedBox(height: 21.0),
                  Row(
                    children: [
                      TextWidget(
                        text: 'IMDb: ${episode.voteAverage}',
                        style:
                            const TextStyle(color: kTextColor, fontSize: 14.0),
                      ),
                      const SizedBox(width: 6.0),
                      const TextWidget(
                        text: '|',
                        style: TextStyle(fontSize: 12.0, color: kTextColor),
                      ),
                      const SizedBox(width: 8.0),
                      TextWidget(
                        text: '${movie.firstAirDate}',
                        style: const TextStyle(
                          fontSize: 14.0,
                          color: kTextColor,
                        ),
                      ),
                      const SizedBox(width: 8.0),
                      const TextWidget(
                        text: '|',
                        style: TextStyle(fontSize: 12.0, color: kTextColor),
                      ),
                      const SizedBox(width: 8.0),
                      TextWidget(
                        text: '${episode.voteAverage}',
                        style: const TextStyle(
                          fontSize: 14.0,
                          color: kTextColor,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 21.0),
                  const Divider(height: 3.0, color: kTextColor),
                  const SizedBox(height: 21.0),
                  TextWidget(
                    text: movie.originalName!,
                    style: Theme.of(context)
                        .textTheme
                        .bodyLarge!
                        .copyWith(color: kTextColor),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class VideoPlayerFutureBuilder extends StatelessWidget {
  const VideoPlayerFutureBuilder({
    Key? key,
    required Future<void> initializeVideoPlayerFuture,
    required VideoPlayerController controller,
  })  : _initializeVideoPlayerFuture = initializeVideoPlayerFuture,
        _controller = controller,
        super(key: key);

  final Future<void> _initializeVideoPlayerFuture;
  final VideoPlayerController _controller;

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _initializeVideoPlayerFuture,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          return VideoPlayer(_controller);
        } else {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}

class _ControlsOverlay extends StatelessWidget {
  const _ControlsOverlay({Key? key, required this.controller})
      : super(key: key);

  final VideoPlayerController controller;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        AnimatedSwitcher(
          duration: const Duration(milliseconds: 50),
          child: controller.value.isPlaying
              ? const SizedBox.shrink()
              : Container(
                  color: Colors.black26,
                  child: const Center(
                    child: CircleAvatar(
                      backgroundColor: kPrimaryColor,
                      radius: 26.0,
                      child: Icon(
                        Icons.play_arrow,
                        color: kBackgroundColor,
                        size: 30.0,
                        semanticLabel: 'Play',
                      ),
                    ),
                  ),
                ),
        ),
        GestureDetector(
          onTap: () {
            controller.value.isPlaying ? controller.pause() : controller.play();
          },
        ),
      ],
    );
  }
}
