import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/popular/data/models/popular_movie.dart';
import 'package:flutter_leal_app/features/provider/popular/presentation/screens/components/hero_popular.dart';


class PageViewPopular extends StatelessWidget {
  final List<PopularMovie> movies;
  final Function nextPage;
  final _pageControler = PageController(
    initialPage: 0,
    viewportFraction: 0.3,
  );
  PageViewPopular({
    Key? key,
    required this.movies,
    required this.nextPage,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    _pageControler.addListener(() {
      if (_pageControler.position.pixels >=
          _pageControler.position.maxScrollExtent - 200) {
        nextPage();
      }
    });
    return SizedBox(
      height: 300,
      child: ListView.builder(
        // pageSnapping: false,
        scrollDirection: Axis.horizontal,
        controller: _pageControler,
        itemCount: movies.length,
        itemBuilder: (context, index) {
          return SizedBox(
            width: 150.0,
            height: 300.0,
            child: HeroPopular(
              movie: movies[index],
            ),
          );
        },
      ),
    );
  }
}
