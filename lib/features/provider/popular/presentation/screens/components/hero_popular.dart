import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/popular/data/models/popular_movie.dart';
import 'package:flutter_leal_app/features/provider/popular/presentation/provider/popular_provier.dart';
import 'package:flutter_leal_app/utils/constants.dart';
import 'package:provider/provider.dart';

import '../../../../widgets/widgets.dart';

class HeroPopular extends StatelessWidget {
  final PopularMovie movie;
  const HeroPopular({
    Key? key,
    required this.movie,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    movie.uniquedId = '${movie.name}';
    return GestureDetector(
      child: Container(
        margin: const EdgeInsets.only(right: 21.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            HeroWidget(
              tag: movie.uniquedId,
              radius: 12.0,
              networkImage: movie.getPosterImg() ,
              assetImage: 'assets/images/no-image.jpg',
              fit: BoxFit.cover,
            ),
            const SizedBox(height: 3.0),
            TextWidget(
              maxLines: 2,
              text: movie.name!,
              style: Theme.of(context)
                  .textTheme
                  .titleLarge!
                  .copyWith(color: kTextColor),
            ),
            const SizedBox(height: 3.0),
            Row(
              children: const [
                RowIconsStarWidget(),
                Spacer(),
              ],
            ),
          ],
        ),
      ),
      onTap: () {
        Provider.of<PopularProvider>(context, listen: false)
            .getMovieDetailsById(movie.id!);
        Navigator.pushNamed(context, "details_popular", arguments: movie);
      },
    );
  }
}
