import 'package:flutter/material.dart';
import 'package:flutter_leal_app/utils/constants.dart';

import '../../../../widgets/widgets.dart';

class SeeAllRowWidget extends StatelessWidget {
  const SeeAllRowWidget({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        const Padding(
          padding: EdgeInsets.symmetric(vertical: 2.0),
          child: TextWidget(
            text: "See All",
            style: TextStyle(fontSize: 21.0, color: kPrimaryColor),
          ),
        ),
        IconButton(
          onPressed: () {},
          icon: const Icon(
            Icons.arrow_forward_ios,
            color: kPrimaryColor,
          ),
        )
      ],
    );
  }
}
