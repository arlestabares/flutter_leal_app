import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/popular/presentation/screens/components/page_view_popular.dart';
import 'package:flutter_leal_app/features/provider/popular/presentation/screens/components/see_all_row_widget.dart';
import 'package:flutter_leal_app/features/provider/widgets/widgets.dart';
import 'package:flutter_leal_app/utils/constants.dart';
import 'package:provider/provider.dart';

import '../provider/popular_provier.dart';

class MainScreenPopularMovies extends StatelessWidget {
  const MainScreenPopularMovies({
    Key? key,
    required this.popular,
  }) : super(key: key);

  final PopularProvider popular;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const TextWidget(
         text: 'Populares',
          style: TextStyle(color: Colors.white, fontSize: 16.0),
        ),
        const SizedBox(height: 12.0),
        StreamBuilder(
          stream: popular.getPopularesStream,
          builder: (context, AsyncSnapshot<dynamic> snapshot) {
            if (snapshot.hasData) {
              return PageViewPopular(
                movies: Provider.of<PopularProvider>(context).getPopularesList,
                nextPage: popular.getPopulares,
              );
            } else {
              return Center(
                child: Column(
                  children: const <Widget>[
                    SizedBox(height: 31.0),
                    CircularProgressIndicator(
                      backgroundColor: kPrimaryColor,
                    ),
                  ],
                ),
              );
            }
          },
        ),
        const SeeAllRowWidget(),
      ],
    );
  }
}
