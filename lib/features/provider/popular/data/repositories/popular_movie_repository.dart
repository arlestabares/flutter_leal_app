import 'dart:async';

import 'package:flutter_leal_app/core/app/data/models/movie_details.dart';
// import 'package:flutter_leal_app/core/app/data/repositories/repository.dart';
import 'package:flutter_leal_app/features/provider/popular/data/datasources/remotedatasource/popular_remote_datasource.dart';
import 'package:flutter_leal_app/features/provider/popular/data/models/popular_movie.dart';

// import '../../../../../core/app/data/datasource/remotedatasource/remote_datasource.dart';

class PopularMovieRepositoryImpl  {
  final PopularMovieDataSourceImpl globalRemoteDataSource;

  PopularMovieRepositoryImpl(this.globalRemoteDataSource);
  
  Future<List<PopularMovie>> getMovie() async {
    return await globalRemoteDataSource.getMovie();
  }

 
  Future<MovieDetailsModel> getDetailsById(int id) async {
    return await globalRemoteDataSource.getDetailsById(id);
  }


  Future<List<Episode>> getSeasons(int id, int seasonNumber) async {
    return await globalRemoteDataSource.getSeasons(id, seasonNumber);
  }

  
  Future<Episode> getEpisode(
      int id, int seasonNumber, int episodeNumber) async {
    return await globalRemoteDataSource.getEpisode(
        id, seasonNumber, episodeNumber);
  }
}
