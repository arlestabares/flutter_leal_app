import 'dart:convert';

// import 'package:flutter_leal_app/core/app/data/datasource/remotedatasource/remote_datasource.dart';
import 'package:flutter_leal_app/features/provider/popular/data/models/popular_movie.dart';

import '../../../../../../core/app/data/models/movie_details.dart';
import '../../../../../../core/app/services/credentials_api.dart';

import 'package:http/http.dart' as http;

class PopularMovieDataSourceImpl {
  bool _popularLoad = false;
  int _popularCountPage = 1;

  Future<List<PopularMovie>> getMovie() async {
    if (_popularLoad) return [];
    _popularLoad = true;
    _popularCountPage++;
    final uri = Uri.https(CredentialsAPI.url, '3/tv/popular', {
      'api_key': CredentialsAPI.apiKey,
      'language': CredentialsAPI.language,
      'page': _popularCountPage.toString()
    });
    final response = await _processData(uri);
    _popularLoad = false;
    return response;
  }

  Future<List<PopularMovie>> _processData(Uri uri) async {
    try {
      final response = await http.get(uri);
      final decodeData = json.decode(response.body);
      final moviesList = MovieList.fromJsonList(decodeData['results']);
      return moviesList.items;
    } catch (e) {
      return List.empty();
    }
  }

  Future<MovieDetailsModel> getDetailsById(int id) async {
    final uri = Uri.https(CredentialsAPI.url, '3/tv/$id', {
      'api_key': CredentialsAPI.apiKey,
      'language': CredentialsAPI.language,
    });
    final response = await _processsResponseById(uri);
    return response;
  }

  Future<List<Episode>> getSeasons(int id, int seasonNumber) async {
    final uri = Uri.https(CredentialsAPI.url, '3/tv/$id/season/$seasonNumber', {
      'api_key': CredentialsAPI.apiKey,
      'language': CredentialsAPI.language,
    });
    final response = await _processSeasonsResponse(uri);

    return response;
  }

  Future<Episode> getEpisode(
      int id, int seasonNumber, int episodeNumber) async {
    final uri = Uri.https(CredentialsAPI.url,
        '3/tv/$id/season/$seasonNumber/episode/$episodeNumber', {
      'api_key': CredentialsAPI.apiKey,
      'language': CredentialsAPI.language,
    });
    final response = await _processEpisodesResponse(uri);

    return response;
  }

  Future<MovieDetailsModel> _processsResponseById(Uri uri) async {
    try {
      final response = await http.get(uri);
      final decodeData = json.decode(response.body);
      final moviesDetails = MovieDetailsModel.fromJson(decodeData);

      return moviesDetails;
    } catch (e) {
      throw Exception('Algo salio mal $e');
    }
  }

  Future<Episode> _processEpisodesResponse(Uri uri) async {
    try {
      final response = await http.get(uri);
      final decodeData = json.decode(response.body);
      final episode = Episode.fromJson(decodeData);

      return episode;
    } catch (e) {
      throw Exception('Algo salio mal $e');
    }
  }

  Future<List<Episode>> _processSeasonsResponse(Uri uri) async {
    try {
      final response = await http.get(uri);
      final decodeData = json.decode(response.body);
      final season = EpisodesList.fromJsonList(decodeData);

      return season.items;
    } catch (e) {
      throw Exception('Algo salio mal $e');
    }
  }
}
