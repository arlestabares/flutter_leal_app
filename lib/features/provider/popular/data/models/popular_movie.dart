class MovieList {
  final List<PopularMovie> items = [];

  MovieList();
  //
  MovieList.fromJsonList(List<dynamic> jsonList) {
    //
    for (var item in jsonList) {
      final movie = PopularMovie.fromJsonMap(item);
      items.add(movie);
    }
  }
}

class PopularMovie {
  String? uniquedId;

  String? posterPath;
  double? popularity;
  int? id;
  String? backdropPath;
  double? voteAverage;
  String? overview;
  DateTime? firstAirDate;
  List<int>? genreIds;
  String? originalLanguage;
  int? voteCount;
  String? originalName;
  String? name;

  PopularMovie({
    this.posterPath,
    this.popularity,
    this.id,
    this.backdropPath,
    this.voteAverage,
    this.overview,
    this.firstAirDate,
    this.genreIds,
    this.originalLanguage,
    this.voteCount,
    this.name,
    this.originalName,
  });

  factory PopularMovie.fromJsonMap(Map<String, dynamic> json) => PopularMovie(
        posterPath: json['poster_path'],
        popularity: json['popularity'] / 1,
        id: json['id'],
        backdropPath: json['backdrop_path'],
        voteAverage: json['vote_average'] / 1,
        overview: json['overview'],
        firstAirDate: DateTime.parse(json["first_air_date"]),
        genreIds: json['genre_ids'].cast<int>(),
        originalLanguage: json['original_lenguage'],
        voteCount: json['vote_count'],
        name: json['name'],
        originalName: json['original_name'],
      );

  getPosterImg() {
    if (posterPath == null) {
      return 'assets/images/no-image.jpg';
    }
    return 'https://image.tmdb.org/t/p/w500/$posterPath';
  }

  getBackgroungImg() {
    if (posterPath == null) {
      return 'assets/images/loading.gif';
    }
    return 'https://image.tmdb.org/t/p/w500/$backdropPath';
  }
}
