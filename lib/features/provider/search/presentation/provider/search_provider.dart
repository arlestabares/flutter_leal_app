import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../../../../../core/app/services/credentials_api.dart';
import '../../../popular/data/models/popular_movie.dart';

class SearchMoviesProvider with ChangeNotifier {
  int popularesPageCount = 1;
  int recommendationsCountPage = 1;
  List<PopularMovie> searchMoviesList = [];

  List<PopularMovie> get getSearchMoviesList => searchMoviesList;

  Future<List<PopularMovie>> getMovie(String query) async {
    popularesPageCount++;
    final url = Uri.https(CredentialsAPI.url, '3/search/movie', {
      'api_key': CredentialsAPI.apiKey,
      'language': CredentialsAPI.language,
      //'page': _popularesPageCount.toString(),
      'query': query
    });

    return await _processsResponse(url);
  }

  Future<List<PopularMovie>> _processsResponse(Uri uri) async {
    try {
      final response = await http.get(uri);
      final decodeData = json.decode(response.body);
      final movies = MovieList.fromJsonList(decodeData);
      return movies.items;
    } catch (e) {
      return List.empty();
    }
  }
}
