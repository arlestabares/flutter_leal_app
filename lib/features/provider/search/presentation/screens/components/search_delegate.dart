import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/search/presentation/provider/search_provider.dart';
import 'package:provider/provider.dart';

import '../../../../popular/data/models/popular_movie.dart';

class DataSearch extends SearchDelegate {
  String seleccion = '';

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
          icon: const Icon(Icons.clear),
          onPressed: () {
            query = '';
          })
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
        icon: AnimatedIcon(
          icon: AnimatedIcons.menu_arrow,
          progress: transitionAnimation,
        ),
        onPressed: () {
          close(context, null);
        });
  }

  @override
  Widget buildResults(BuildContext context) {
    return Center(
      child: Container(
        height: 100.0,
        width: 100.0,
        color: Colors.blueAccent,
        child: Text(seleccion),
      ),
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final moviesProvider = Provider.of<SearchMoviesProvider>(context);

    if (query.isEmpty) {
      return Container();
    }

    return FutureBuilder(
      future: moviesProvider.getMovie(query),
      builder: (BuildContext context, AsyncSnapshot<List<PopularMovie>> snapshot) {
        if (snapshot.hasData) {
          final movies = snapshot.data;

          return ListView(
              children: movies!.map((movie) {
            return ListTile(
              leading: FadeInImage(
                image: NetworkImage(movie.getPosterImg()),
                placeholder: const AssetImage('assets/images/no-image.jpg'),
                width: 50.0,
                fit: BoxFit.contain,
              ),
              title: Text(movie.name!),
              subtitle: Text(movie.originalName!),
              onTap: () {
                close(context, null);
                movie.uniquedId = '';
                Navigator.pushNamed(context, 'detalle', arguments: movie);
              },
            );
          }).toList());
        } else {
          return const Center(child: CircularProgressIndicator());
        }
      },
    );
  }
}
