import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/search/presentation/screens/components/search_delegate.dart';
import 'package:flutter_leal_app/features/provider/search/presentation/screens/search_screen.dart';
import 'package:flutter_leal_app/features/provider/widgets/widgets.dart';

class SearchPage extends StatelessWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFF191919),
        title: const Text('Search'),
        actions: [
          IconButtonWidget(
            icon: const Icon(
              Icons.search,
              size: 36.0,
            ),
            onPressed: () {
              showSearch(context: context, delegate: DataSearch());
            },
          )
        ],
      ),
      body: const SearchScreen(),
    );
  }
}
