part of 'recent_bloc.dart';

abstract class RecentState extends Equatable {
  const RecentState();  

  @override
  List<Object> get props => [];
}
class RecentInitial extends RecentState {}
