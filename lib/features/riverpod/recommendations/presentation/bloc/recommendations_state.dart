part of 'recommendations_bloc.dart';

abstract class RecommendationsState extends Equatable {
  const RecommendationsState();  

  @override
  List<Object> get props => [];
}
class RecommendationsInitial extends RecommendationsState {}
