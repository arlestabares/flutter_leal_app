import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'recommendations_event.dart';
part 'recommendations_state.dart';

class RecommendationsBloc extends Bloc<RecommendationsEvent, RecommendationsState> {
  RecommendationsBloc() : super(RecommendationsInitial()) {
    on<RecommendationsEvent>((event, emit) {
    });
  }
}
