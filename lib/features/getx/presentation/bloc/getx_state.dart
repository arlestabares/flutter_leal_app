part of 'getx_bloc.dart';

abstract class GetxState extends Equatable {
  const GetxState();  

  @override
  List<Object> get props => [];
}
class GetxInitial extends GetxState {}
