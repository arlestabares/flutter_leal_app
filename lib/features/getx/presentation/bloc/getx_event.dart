part of 'getx_bloc.dart';

abstract class GetxEvent extends Equatable {
  const GetxEvent();

  @override
  List<Object> get props => [];
}
