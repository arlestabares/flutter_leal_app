import 'package:flutter/material.dart';

const kBackgroundColor = Color(0xFF191919);
const kBackgroundColorButton = Color(0xFFFFFFFF);
const kTextColor = Color(0xFF8C8C8C);
const kTextColorWhite = Color(0xFFFFFFFF);
const kTextLightColor = Color(0xFF9A9BB2);
const kPrimaryColor = Color(0xFFFFD233);
const kColorTransparent = Colors.transparent;

const kDefaultPadding = 20.0;

const kDefaultShadow = BoxShadow(
  offset: Offset(0, 4),
  blurRadius: 4,
  color: Colors.black26,
);
