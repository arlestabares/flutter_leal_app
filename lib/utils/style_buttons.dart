import 'package:flutter/material.dart';

final ButtonStyle textButtonStyle = TextButton.styleFrom(
  primary: Colors.black87,
  minimumSize: const Size(88, 36),
  padding: const EdgeInsets.symmetric(horizontal: 16.0),
  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(18.0)),
  ),
);

final ButtonStyle elevatedButtonStyleYellow = ElevatedButton.styleFrom(
  onPrimary: Colors.black87,
  primary: const Color(0xFFFFD233),
  minimumSize: const Size(46, 46),
  padding: const EdgeInsets.symmetric(horizontal: 21),
  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(26.0)),
  ),
);

final ButtonStyle elevatedButtonStyleWhite = ElevatedButton.styleFrom(
  onPrimary: const Color(0xFF8C8C8C),
  primary: const Color(0xFFFFFFFF),
  minimumSize: const Size(46, 46),
  padding: const EdgeInsets.symmetric(horizontal: 21),
  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(26.0)),
  ),
);

final ButtonStyle outlineButtonStyle = OutlinedButton.styleFrom(
  primary: Colors.black87,
  onSurface: const Color(0xFFFFD233),
  backgroundColor: const Color(0xFFFFD233),
  minimumSize: const Size(88, 36),
  padding: const EdgeInsets.symmetric(horizontal: 16),
  shape: const RoundedRectangleBorder(
    borderRadius: BorderRadius.all(Radius.circular(18.0)),
  ),
);

const TextStyle optionStyle =
    TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
