import 'package:flutter/material.dart';
import 'package:flutter_leal_app/core/network/network_info.dart';
import 'package:flutter_leal_app/routes/app_routes.dart';
import 'package:flutter_leal_app/utils/constants.dart';
import 'core/app/services/injector_container.dart' as di;
import 'features/provider/providers/multiprovider_widget.dart';

final internetChecker = CheckInternetConnection();
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiproviderWidget(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData.from(colorScheme: const ColorScheme.light()).copyWith(
          // textButtonTheme: TextButtonThemeData(style: textButtonStyle),
          // elevatedButtonTheme:
          //     ElevatedButtonThemeData(style: elevatedButtonStyleYellow),
          // outlinedButtonTheme:
          //     OutlinedButtonThemeData(style: outlineButtonStyle),
          scaffoldBackgroundColor: const Color(0xFF191919),
          appBarTheme:
              const AppBarTheme(color: Color(0xFF191919), elevation: 0.0),
          primaryColor: kPrimaryColor,
        ),
        // initialRoute: 'home_page',
        initialRoute: 'login_page',
        // initialRoute: 'recent_moviesPage',
        routes: appRoutes,
      ),
    );
  }
}
