import 'package:flutter/material.dart';
import 'package:flutter_leal_app/features/provider/popular/presentation/screens/popular_episodes/popular_watch_now.dart';
import 'package:flutter_leal_app/features/provider/recent/presentation/pages/recent_new.dart';
import 'package:flutter_leal_app/features/provider/recommendations/presentation/screens/recommendations_episodes/recommended_movie_episodes.dart';

import '../features/provider/favorites/presentation/pages/favorites_page.dart';
import '../features/provider/home/presentation/pages/bottom_navigation_bar_page.dart';
import '../features/provider/login/presentation/pages/login_page.dart';
import '../features/provider/popular/presentation/pages/popular_movie_page.dart';
import '../features/provider/recent/presentation/pages/recent_movies_page.dart';
import '../features/provider/recommendations/presentation/pages/recommendations_movies_page.dart';

Map<String, Widget Function(BuildContext)> appRoutes = {
  'login_page': (_) => const LoginPage(),
  'home_page': (_) => const BottomNavigationBarPage(),
  'details_popular': (_) => const PopularMoviesPage(),
  'details_recommendations': (_) => const RecommendationsMoviesPage(),
  "recommendations_watch_now": (_) => const RecommendedMovieEpisodes(),
  "popular_watch_now": (_) => const PopularEpisodes(),

  'favoritesPage': (_) => const FavoritesPage(),
  //
  'recent_moviesPage': (_) => const RecentMoviesPage(),
  // 'recent_details_page': (_) => const RecentEpisodesPage(),
  'last_episode_to_air_page': (_) => const DetailsLastEpisodeToAirPage(),
};
